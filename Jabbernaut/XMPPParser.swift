import UIKit



open class XMPPParser: NSObject {

   var root = NSDictionary()
   var TEXTNODE_KEY = ""
   
   
   override init() {
      super.init()
      TEXTNODE_KEY = xmppclient.jabberconnector.xmlreader.textnode_KEY
   }

   
   convenience init(login:String, server:String, password:String) {
      self.init()
      
      xmppclient.model.login = login         //"bzz"
      xmppclient.model.server = server       //"xmpp.jp"
      xmppclient.model.password = password   //"134679"
   }


}




extension XMPPParser {
   

   
   func checkInitialServerResponse() {
      
      if "urn:ietf:params:xml:ns:xmpp-tls" == root.getString(["stream:features", "starttls", "xmlns"]) {
         
         if root.getObject(["stream:features", "starttls", "required"]) != nil {
            Notifier.post(.tlsRequired)
            return
         } else {
            Notifier.post(.tlsFeatureFound)
         }
      }


      
      if "urn:ietf:params:xml:ns:xmpp-sasl" == root.getString(["stream:features", "mechanisms", "xmlns"]) {
         
         if let mechanism = root.getObject(["stream:features", "mechanisms", "mechanism"]) {
            print(mechanism)
            
            var arr = [String]()
            if mechanism.isKind(of: NSArray.self) {
                for m in mechanism as! [[String:Any]] {
                  let str = m[TEXTNODE_KEY] as? String
                  arr.append(str!)
               }
            } else {
               let m = mechanism
               let str = m[TEXTNODE_KEY] as? String
               arr.append(str!)
            }
            Notifier.post(.authMechanismsFound, userInfo:["mechanisms" : arr])
            
         }
      }
      

//checkRegistrationRequestResponse
      if root.getString(["iq", "id"]) == "reg2" {
         let type = root.getString(["iq", "type"])
         if type == "result" {
            Notifier.post(.registrationSucceed, userInfo: ["server":xmppclient.model.server])
         }
         if type == "error" {
            let error_type = root.getString(["iq", "error", "type"])
            if error_type == "wait" {
               if root.getString(["iq", "error", "resource-constraint", "xmlns"]) == "urn:ietf:params:xml:ns:xmpp-stanzas" {
                  Notifier.post(.registrationFailedTooFast, userInfo: ["server":xmppclient.model.server])
               }
            }
            if error_type == "cancel" {
               if root.getString(["iq", "error", "conflict", "xmlns"]) == "urn:ietf:params:xml:ns:xmpp-stanzas" {
                  Notifier.post(.registrationFailedTaken, userInfo: ["server":xmppclient.model.server])
               }
               if root.getString(["iq", "error", "not-allowed", "xmlns"]) == "urn:ietf:params:xml:ns:xmpp-stanzas" {
                  Notifier.post(.registrationFailedNotAllowed, userInfo: ["server":xmppclient.model.server])
               }
            }
            if error_type == "auth" {
               if root.getString(["iq", "error", "forbidden", "xmlns"]) == "urn:ietf:params:xml:ns:xmpp-stanzas" {
                  print("REGISTRATION ERROR: FORBIDDEN")
                  Notifier.post(.registrationFailedNotAllowed, userInfo: ["server":xmppclient.model.server])
               }
            }
         }
      }
      
//checkAuthChallenge
      if "urn:ietf:params:xml:ns:xmpp-sasl" == root.getString(["challenge", "xmlns"]) {
         let instr = root.getString(["challenge", TEXTNODE_KEY]).fromBase64()
         var dic = Dictionary<String, String>()
         let arr = instr.components(separatedBy: ",")
         for str in arr {
            for i in 0..<str.characters.count {
               if str.characters[str.characters.index(str.startIndex, offsetBy: i)] == "=" {
                  let key = str.substring(to: str.characters.index(str.startIndex, offsetBy: i))
                  dic[key] = str.substring(from: str.characters.index(str.startIndex, offsetBy: i+1))
                  break
               }
            }
         }
         ///DIGEST_MD5
         if let nonceQuotes = dic["nonce"],
            let qop = dic["qop"],
            let algorithm = dic["algorithm"] {
            let nonce = nonceQuotes.replacingOccurrences(of: "\"", with: "", options: NSString.CompareOptions.literal, range: nil)
            
            if qop != "\"auth\"" {
               return
            }
            var realm = dic["realm"]
            if realm == nil {
               realm = ""
            }
            var charset = dic["charset"]
            if charset == nil {
               charset = "utf-8"
            }
            xmppclient.sender.auth_solve_challenge_DIGEST_MD5(realm:realm!, nonce:nonce, qop:qop, charset:charset!, algorithm:algorithm)
         }
         if let _ = dic["rspauth"] {   //authDIGEST_MD5 final challenge
            xmppclient.sender.auth_DIGEST_MD5_solve_final_challenge()
         }
         //SCRAM_SHA1
         if let combinedNonce = dic["r"],
            let salt = dic["s"],
            let i = dic["i"] {
            xmppclient.sender.auth_solve_challenge_SCRAM_SHA1(combinedNonce:combinedNonce, salt:salt, countStr:i)
         }
      }
      
//checkAuthResponse
      if "urn:ietf:params:xml:ns:xmpp-sasl" == root.getString(["success", "xmlns"]) {
         Notifier.post(.authSucceed, userInfo: ["server":xmppclient.model.server])
      }
      if "urn:ietf:params:xml:ns:xmpp-sasl" == root.getString(["failure", "xmlns"]) {
         Notifier.post(.authFailed, userInfo: ["server":xmppclient.model.server])
      }
      
      
      
      
//checkProceedTLS
      if "urn:ietf:params:xml:ns:xmpp-tls" == root.getString(["proceed", "xmlns"]) {
         Notifier.post(.proceedWithTLS)
      }

   }

   
   
   func checkServerResponse() {
      
      if "urn:ietf:params:xml:ns:xmpp-bind" == root.getString(["stream:features", "bind", "xmlns"]) {
         Notifier.post(.bindFeatureFound)
      }
      
      
      switch root.getString(["iq", "type"]) {
         
      case "set":
         let item_jid = root.getString(["iq", "query", "item", "jid"])
         let subscription = root.getString(["iq", "query", "item", "subscription"])
         if root.getString(["iq", "query", "item", "ask"]) == "subscribe" {
            print("SUBSCRIPTION STATUS FOR USER \(item_jid) IS \(subscription)")
         }
         if subscription == "from" {
            print("You are subscribed to \(item_jid), \(item_jid) is NOT subscribed to you")
         }
         if subscription == "to" {
            print("You are NOT subscribed to \(item_jid), \(item_jid) is subscribed to you")
         }
         
         if subscription == "both" {
            print("YOU ARE SUBSCRIBED TO \(item_jid), \(item_jid) SUBSCRIBED TO YOU")
         }
         
         if subscription == "none" {
            print("\(item_jid) AND YOU ARE UNSUBSCRIBED")
         }
         

      case "error":
         if root.getString(["iq", "id"]) == "BIND" {
            print("BINDING ERROR")
         }
         if root.getString(["iq", "id"]) == "PING" {
            print("PING ERROR")
         }
         if root.getString(["iq", "id"]) == "DELETE_NODE" {
            print("DELETE NODE ERROR")
         }
         

      case "result":
         if root.getString(["iq", "id"]) == "BIND" {
            Notifier.post(.boundResource)
         }
         if root.getString(["iq", "id"]) == "PING" {
            Notifier.post(.gotPong)
         }
         if root.getString(["iq", "id"]) == "DELETE_NODE" {
            print("DELETE NODE SUCCEED")
         }
         if "urn:ietf:params:xml:ns:xmpp-session" == root.getString(["iq", "session", "xmlns"]) {
            if xmppclient.authmodel.jabber_authorised {
               Notifier.post(.gotSession)
            }
         }
         if "http://jabber.org/protocol/pubsub" == root.getString(["iq", "pubsub", "xmlns"]) {
            let node = root.getString(["iq", "pubsub", "publish", "node"])
            let id = root.getString(["iq", "pubsub", "publish", "item", "id"])
            print("PUBSUB NODE \(node) PUBLISHED,  id: \(id)")
         }
         
         if "vcard-temp" == root.getString(["iq", "vCard", "xmlns"]) {
            let from_jid = root.getString(["iq", "from"]).components(separatedBy: "/")[0]

            let name = root.getString(["iq", "vCard", "FN", TEXTNODE_KEY])
            let position = root.getString(["iq", "vCard", "DESC", TEXTNODE_KEY])
            let address_text = root.getString(["iq", "vCard", "ADR", "WORK", TEXTNODE_KEY])
            let pcode_text = root.getString(["iq", "vCard", "ADR", "PCODE", TEXTNODE_KEY])
            let avatar = root.getString(["iq", "vCard", "PHOTO", "BINVAL", TEXTNODE_KEY])
            
            var buddy : Buddy!
            for key in appdata.buddies.allKeys {
               buddy = Buddy(key: key as! String)
               
               if buddy.string == Buddy(key: from_jid).string {
                  buddy.name = name
                  buddy.position = position
                  buddy.address = address_text
                  if pcode_text != "" {
                     buddy.APNSToken = pcode_text
                  }
                  
                  if address_text != "" {
                     appdata.avatars[address_text] = avatar
                  } else {
                     appdata.avatars[from_jid] = avatar
                  }
                  
                  
                  buddy.save()

                  xmppclient.model.friendsGotVCARD.insert(from_jid)
                  Notifier.post(.gotVCARD, userInfo: ["from" : from_jid])
               }
            }
         }

         if let item = root.getObject(["iq", "query", "item"]) as? NSArray {
            xmppclient.model.roster.removeAllObjects()
            
            var itemType = 0
            for b in item as! [[String:Any]] {
               if let jid = b["jid"] as? String {
                  if jid.range(of: "@") == nil {
                     var name = b["name"] as? String
                     if name == nil {
                        name = ""
                     }
                     if let node = b["node"] as? String, jid == "pubsub.\(xmppclient.model.server)" {
                        xmppclient.model.pubsubnodes[node] = name
                        itemType = 1
                     }
                  } else {
                     
                     if let ask = b["ask"] as? String, ask == "subscribe" {
                        //               print("REQUEST SENT: " + jid)
                     } else if let subscription = b["subscription"] as? String {
                        xmppclient.model.roster[jid] = subscription
                        itemType = 2
                     }
                  }
               }
            }
            if itemType == 1 {
               Notifier.post(.gotPostsubNodes)
            } else if itemType == 2 {
               Notifier.post(.gotRoster)
            } else {
            }
         }

         
      default: break
      }
   
      
      let from_jid = root.getString(["presence", "from"]).components(separatedBy: "/")[0]
      if from_jid != "" {
         let type = root.getString(["presence", "type"])
         let show = root.getString(["presence", "show", TEXTNODE_KEY])
         Notifier.post(.gotPresence, userInfo: ["from": from_jid, "type" : type, "show" : show])
      }


      let message_from = root.getString(["message", "from"]).components(separatedBy: "/")[0]
      if message_from != "" {
         let type = root.getString(["message", "type"])
         if type == "error" {
            Notifier.post(.messageDeliveryError, userInfo: ["from" : message_from])
         } else {
            let body = root.getString(["message", "body", TEXTNODE_KEY])
            if body != "" {
               let messageID = root.getString(["message", "id"])
               if messageID != "" {
                  Notifier.post(.gotMessage, userInfo: ["from":message_from, "body":body, "messageID":messageID])
               } else {
                  Notifier.post(.gotMessage, userInfo: ["from":message_from, "body":body])
               }
            } else {
               let recieved_messageID = root.getString(["message", "recieved", "id"])
               if recieved_messageID != "" {
                  Notifier.post(.messageReceiptConfirmed, userInfo: ["messageID":recieved_messageID])
               }
            }
         }
      }
      
      

   }
   

   
   
   
}
