//
//  HEXColor.swift
//  Dashboard
//
//  Created by Mikhail Baynov on 2/5/16.
//  Copyright © 2016 Mikhail Baynov. All rights reserved.
//

import UIKit

public enum UIColorInputError : Error {
   case missingHashMarkAsPrefix,
   unableToScanHexValue,
   mismatchedHexStringLength
}

extension UIColor {
   public convenience init(hex: UInt32, alpha: CGFloat = 1) {
      let divisor = CGFloat(255)
      let red     = CGFloat((hex & 0xFF0000) >> 16) / divisor
      let green   = CGFloat((hex & 0x00FF00) >>  8) / divisor
      let blue    = CGFloat( hex & 0x0000FF       ) / divisor
      self.init(red: red, green: green, blue: blue, alpha: alpha)
   }
   
   /**
    The six-digit hexadecimal representation of color with alpha of the form #RRGGBBAA.
    
    - parameter hex8: Eight-digit hexadecimal value.
    */
   public convenience init(hexA: UInt32) {
      let divisor = CGFloat(255)
      let red     = CGFloat((hexA & 0xFF000000) >> 24) / divisor
      let green   = CGFloat((hexA & 0x00FF0000) >> 16) / divisor
      let blue    = CGFloat((hexA & 0x0000FF00) >>  8) / divisor
      let alpha   = CGFloat( hexA & 0x000000FF       ) / divisor
      self.init(red: red, green: green, blue: blue, alpha: alpha)
   }
   
   /**
    The rgba string representation of color with alpha of the form #RRGGBBAA/#RRGGBB, throws error.
    
    - parameter rgba: String value.
    */
   public convenience init(rgba_throws rgba: String) throws {
      guard rgba.hasPrefix("#") else {
         throw UIColorInputError.missingHashMarkAsPrefix
      }
      
      guard let hexString: String = rgba.substring(from: rgba.characters.index(rgba.startIndex, offsetBy: 1)),
         var   hexValue:  UInt32 = 0, Scanner(string: hexString).scanHexInt32(&hexValue) else {
            throw UIColorInputError.unableToScanHexValue
      }
      
      guard hexString.characters.count  == 6
         || hexString.characters.count == 8 else {
            throw UIColorInputError.mismatchedHexStringLength
      }
      
      switch (hexString.characters.count) {
      case 6:
         self.init(hex: hexValue)
      default:
         self.init(hexA: hexValue)
      }
   }
   
   /**
    The rgba string representation of color with alpha of the form #RRGGBBAA/#RRGGBB, fails to default color.
    
    - parameter rgba: String value.
    */
   public convenience init(rgba: String, defaultColor: UIColor = UIColor.clear) {
      guard let color = try? UIColor(rgba_throws: rgba) else {
         self.init(cgColor: defaultColor.cgColor)
         return
      }
      self.init(cgColor: color.cgColor)
   }
   
   /**
    Hex string of a UIColor instance.
    
    - parameter rgba: Whether the alpha should be included.
    */
   public func hexString(_ includeAlpha: Bool) -> String {
      var r: CGFloat = 0
      var g: CGFloat = 0
      var b: CGFloat = 0
      var a: CGFloat = 0
      self.getRed(&r, green: &g, blue: &b, alpha: &a)
      
      if (includeAlpha) {
         return String(format: "#%02X%02X%02X%02X", Int(r * 255), Int(g * 255), Int(b * 255), Int(a * 255))
      } else {
         return String(format: "#%02X%02X%02X", Int(r * 255), Int(g * 255), Int(b * 255))
      }
   }
   
   open override var description: String {
      return self.hexString(true)
   }
   
   open override var debugDescription: String {
      return self.hexString(true)
   }
}


extension UIColor {
   var invertedColor: UIColor {
      var r: CGFloat = 0.0, g: CGFloat = 0.0, b: CGFloat = 0.0, a: CGFloat = 0.0
      UIColor.red.getRed(&r, green: &g, blue: &b, alpha: &a)
      return UIColor(red: 1 - r, green: 1 - g, blue: 1 - b, alpha: a) // Assuming you want the same alpha value.
   }
}


extension String {
   func dataFromHexadecimalString() -> Data? {
      let data = NSMutableData(capacity: characters.count / 2)
      
      let regex = try! NSRegularExpression(pattern: "[0-9a-f]{1,2}", options: .caseInsensitive)
      regex.enumerateMatches(in: self, options: [], range: NSMakeRange(0, characters.count)) { match, flags, stop in
         let byteString = (self as NSString).substring(with: match!.range)
         let num = UInt8(byteString.withCString { strtoul($0, nil, 16) })
         data?.append([num], length: 1)
      }
      return data as! Data
   }
}

//extension Data {
//   func hexadecimalString() -> String {
//      var string = ""
//      var byte: UInt8 = 0
//      
//      for i in 0 ..< count {
//         getBytes(byte, range: NSMakeRange(i, 1))
//         string += String(format: "%02X", byte)
//      }
//      return string
//   }
//}



extension String
{
   func fromBase64() -> String
   {
      let data = Data(base64Encoded: self, options: NSData.Base64DecodingOptions(rawValue: 0))
      return String(data: data!, encoding: String.Encoding.utf8)!
   }
   
   func toBase64() -> String
   {
      let data = self.data(using: String.Encoding.utf8)
      return data!.base64EncodedString(options: [])
   }
   
   func sha1() -> String {
      let data = self.data(using: String.Encoding.utf8)!
      var digest = [UInt8](repeating: 0, count: Int(CC_SHA1_DIGEST_LENGTH))
      CC_SHA1((data as NSData).bytes, CC_LONG(data.count), &digest)
      let hexBytes = digest.map { String(format: "%02hhx", $0) }
      return hexBytes.joined(separator: "")
   }
   
   
   
   static func randomStringWithLength (_ len : Int) -> String {
      let letters : NSString = "abcdefghijklmnopqrstuvwxyz"
      let randomString : NSMutableString = NSMutableString(capacity: len)
      for i in (0..<len) {
         //      for (var i=0; i < len; i++){
         let length = UInt32 (letters.length)
         let rand = arc4random_uniform(length)
         randomString.appendFormat("%C", letters.character(at: Int(rand)))
      }
      return String(randomString)
   }
   
   static func randomNumberWithLength (_ len : Int) -> String {
      let letters : NSString = "0123456789"
      let randomString : NSMutableString = NSMutableString(capacity: len)
      for i in (0..<len) {
         //      for (var i=0; i < len; i++){
         let length = UInt32 (letters.length)
         let rand = arc4random_uniform(length)
         randomString.appendFormat("%C", letters.character(at: Int(rand)))
      }
      return String(randomString)
   }
   
}



extension Date {
   func dateStringWithFormat(_ format: String) -> String {
      let df = DateFormatter()
      df.dateFormat = format
      return df.string(from: self)
   }
   
   func UTCString() -> String {
      let df = DateFormatter()
      df.timeZone = TimeZone(abbreviation: "UTC")
      df.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
      return df.string(from: self)
   }
/*//date from UTC string
    let df = NSDateFormatter()
    df.timeZone = NSTimeZone(abbreviation: "UTC")
    df.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
    var dstr = ""
    if let d = df.dateFromString(time) {
    dstr = d.dateStringWithFormat("HH:mm")
    }
*/

}



extension NSDictionary {
   func getString(_ keys: [String]) -> String {
      var d = self
      for i in 0..<keys.count {
         if let tmp = d[keys[i]]  {
            if (tmp as AnyObject).isKind(of: NSDictionary.self) {
               d = tmp as! NSDictionary
               
            } else if (tmp as AnyObject).isKind(of: NSString.self) {
               return tmp as! String
            }
         }
      }
      return ""
   }
   
   func getObject(_ keys : [String]) -> AnyObject? {
      var d = self
      for i in 0..<keys.count {
         if let tmp = d[keys[i]]  {
            if (tmp as AnyObject).isKind(of: NSDictionary.self) {
               d = tmp as! NSDictionary
            }
            if i + 1 == keys.count {
               return tmp as AnyObject
            }
         }
         
      }
      return nil
   }
}



