//
//  XMLReader.h
//  XMLReader
//
//  Created by Mikhail Baynov on 30/05/16.
//  Copyright © 2016 Mikhail Baynov. All rights reserved.
//

/*  If tags are not closed:  appends text, returns nil
 else:  returns NSDictionary
 
 TEXTNODE_KEY=... must always go last!
 */

#import <Foundation/Foundation.h>

@interface XMLReader : NSObject


@property (nonatomic, strong) NSString* TEXTNODE_KEY;
@property (nonatomic, strong) NSString* text;
@property (nonatomic) size_t MAX_BUFFER_SIZE;


+ (instancetype)shared;


- (NSDictionary *)dictionaryForXMLString:(NSString *)string;


- (NSDictionary *)addText:(NSString *)text;
- (NSDictionary *)dictionaryFromText;


@end
