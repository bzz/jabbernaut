import UIKit


class WelcomeVC : UIViewController {

   var bedButton : UIButton!
   

   override func viewDidLoad() {
      super.viewDidLoad()
      
      
      
//      if let background = UIImage(named: "background.png") {
//         view.backgroundColor = UIColor(patternImage: background)
//      }
      
      
      let welcomeLabel = UILabel(frame: CGRect(x: 88, y: 70, width: screenWidth-176, height: 90))
      welcomeLabel.text = "Welcome to \nJABBERNAUT"
      welcomeLabel.numberOfLines = 2
      welcomeLabel.textAlignment = .center
      welcomeLabel.textColor = UIColor(hex: 0x000000)
      welcomeLabel.font = UIFont(name: "HelveticaNeue-Thin", size: 24)
      view.addSubview(welcomeLabel)
      
      
      //bed
      bedButton = UIButton(frame: CGRect(x: 0, y: 170, width: screenWidth, height: 104))
      bedButton.backgroundColor = UIColor(hex: 0xf7f7f7)
      bedButton.addTarget(self, action:#selector(WelcomeVC.bedButtonPressed), for: UIControlEvents.touchDown)
      bedButton.layer.borderColor = UIColor(hex: 0xdcdcdc).cgColor
      bedButton.layer.borderWidth = 1
      view.addSubview(bedButton)
      
      let textLabel = UILabel(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 30))
      textLabel.backgroundColor = UIColor(hex: 0xf7f7f7)
      textLabel.textAlignment = .center
      textLabel.text = "Your ID:"
      textLabel.textColor = UIColor(hex: 0x000000)
      textLabel.font = UIFont(name: "HelveticaNeue-Thin", size: 21)
      bedButton.addSubview(textLabel)
      
      let addressLabel = UILabel(frame: CGRect(x: 0, y: 30, width: screenWidth, height: 30))
      addressLabel.backgroundColor = UIColor(hex: 0xf7f7f7)
      addressLabel.textAlignment = .center
      addressLabel.text = appdata.addressString
      addressLabel.textColor = UIColor(hex: 0x000000)
      addressLabel.font = UIFont(name: "Courier", size: 12)
      bedButton.addSubview(addressLabel)
      
      let proceedButton = UIButton(frame: CGRect(x: 0, y: 60, width: screenWidth, height: 44))
      proceedButton.setTitle("PROCEED", for: UIControlState())
      proceedButton.setTitleColor(UIColor.black, for: UIControlState())
      proceedButton.titleLabel?.font = UIFont(name: "HelveticaNeue-Regular", size: 18)
      proceedButton.backgroundColor = UIColor(hex: 0xf9f9f9)
      proceedButton.addTarget(self, action:#selector(proceedButtonPressed), for: UIControlEvents.touchUpInside)
      bedButton.addSubview(proceedButton)

   }
   
   func proceedButtonPressed() {
      if appdata.addressString != "" {
         appDelegate.proceedToNavigation()
      }
   }
   func bedButtonPressed() {
      print("bedButtonPressed")
      appDelegate.window?.rootViewController = SecretKeyVC()
   }
}
