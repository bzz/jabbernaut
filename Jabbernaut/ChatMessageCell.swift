import UIKit


class ChatMessageCell: UITableViewCell {
   var view = UIView()
   
   override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
      super.init(style: style, reuseIdentifier: reuseIdentifier)
      self.backgroundColor = UIColor.clear
      self.selectionStyle = .none
   }
   
   required init?(coder aDecoder: NSCoder) {
      super.init(coder: aDecoder)
   }
}


class MessageView: UIView {
   var iconView = UIImageView()
   var tail = UIImageView()
   
   var stringLabel : UILabel!
   var timeLabel : UILabel!
   var envelope : UIImageView!
  
   
   init(string:String, time:String, icon:UIImage?, outgoing:Bool, read:Bool) {
      super.init(frame: CGRect(x:0, y:0, width:UIScreen.main.bounds.width, height:80))
      
      let bubble = UIView()
      bubble.backgroundColor = UIColor(hex: 0xffffff)
      bubble.layer.cornerRadius = 10
      self.addSubview(bubble)
      
      stringLabel = UILabel(frame:CGRect(x:12, y:10, width:screenWidth - 140, height:30))
      stringLabel.numberOfLines = 0
      stringLabel.text = string
      stringLabel.textColor = UIColor(hex: 0x000000)
      stringLabel.font = UIFont(name: "HelveticaNeue", size: 13)
      stringLabel.sizeToFit()
      stringLabel.frame = CGRect(x:12, y:10, width:screenWidth - 140, height:stringLabel.frame.height)

      
      
      
      let df = DateFormatter()
      df.timeZone = TimeZone(abbreviation: "UTC")
      df.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
      var dstr = ""
      if let d = df.date(from: time) {
         dstr = d.dateStringWithFormat("HH:mm")
      }

      timeLabel = UILabel(frame:CGRect(x:25, y:16 + stringLabel.frame.height, width:screenWidth - 130, height:12))
      timeLabel.numberOfLines = 1
      timeLabel.text = dstr
      timeLabel.textColor = UIColor(hex: 0x9e9e9e)
      timeLabel.font = UIFont(name: "HelveticaNeue", size: 10)
      bubble.addSubview(timeLabel)

      if outgoing {
         if read {
            envelope = UIImageView(image: UIImage(named: "envelope_open"))
         } else {
            envelope = UIImageView(image: UIImage(named: "envelope"))
         }
         envelope.frame = CGRect(x:12, y:16 + stringLabel.frame.height, width:10, height:10)
         bubble.addSubview(envelope)

         iconView.frame = CGRect(x:screenWidth - 15 - 29, y:4, width:26, height:26)
         bubble.frame = CGRect(x:68, y:0, width:stringLabel.frame.width + 20, height:stringLabel.frame.height + 38)
         tail = UIImageView(image: UIImage(named: "bubble_tail_right"))
         tail.frame = CGRect(x:bubble.frame.width + 68 - 14, y:0, width:tail.frame.width, height:tail.frame.height)
      } else {
         timeLabel.frame.origin.x = 16
         
         iconView.frame = CGRect(x:15, y:4, width:29, height:29)
         bubble.frame = CGRect(x:44 + 8, y:0, width:stringLabel.frame.width + 20, height:stringLabel.frame.height + 38)
         tail = UIImageView(image: UIImage(named: "bubble_tail_left"))
         tail.frame = CGRect(x:44, y:0, width:tail.frame.width, height:tail.frame.height)
      }
      iconView.backgroundColor = UIColor(hex: 0xf3f3f3)
      iconView.layer.cornerRadius = iconView.frame.width/2.0
      iconView.layer.masksToBounds = true
      iconView.image = icon

      self.addSubview(tail)
      self.addSubview(iconView)
      bubble.addSubview(stringLabel)
      
      self.frame = CGRect(x:0, y:0, width:screenWidth, height:bubble.frame.height + 10)
   }
   
   required init?(coder aDecoder: NSCoder) {
      super.init(coder: aDecoder)
   }
}

