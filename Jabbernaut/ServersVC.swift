import UIKit

class ServersVC: UIViewController {
    
   var controller : ServersVC?

   var tableView = UITableView()
   var tableViewArray: [ServerView] = []

   var searchView = UIView()
   var searchField = UITextField()
   
   var parentVC : NavigationVC!
   override func viewDidLoad() {
      super.viewDidLoad()
      parentVC = parent as! NavigationVC
      view.frame = parentVC.mainViewBed.bounds
      
      view.backgroundColor = UIColor(hexA:0xffffff15)
      
      
      searchView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 44))
      searchView.backgroundColor = UIColor(hex:0xc2c2c5)      
      UITextField.appearance().tintColor = UIColor(hex:0xc2c2c5)
      searchField = UITextField()
      searchField.placeholder = "Add new server"
      searchField.frame = CGRect(x: 7, y: 7, width: view.frame.width-14, height: 30)
      searchField.layer.cornerRadius = 5
      searchField.backgroundColor = UIColor.white
      searchField.textAlignment = .center
      searchField.autocapitalizationType = .none
      searchField.autocorrectionType = .no
      searchField.keyboardType = .emailAddress
      searchField.delegate = self
      searchView.addSubview(searchField)
      view.addSubview(searchView)
      
      
      
      tableView = UITableView(frame: CGRect(x: 0, y: 44, width: view.frame.width, height: view.frame.height - 44))
      tableView.delegate = self
      tableView.dataSource = self
      tableView.register(ServerCell.self, forCellReuseIdentifier: String(describing: ServerCell.self))
      tableView.backgroundColor = UIColor.clear
      tableView.showsVerticalScrollIndicator = false
      tableView.separatorStyle = .none
      tableView.rowHeight = UITableViewAutomaticDimension;
      
      view.addSubview(tableView)
      
      
      parentVC.removeAddButton()
      parentVC.setupTitleButton("Servers", titleColor: UIColor(hex: 0x000000), font: UIFont(name: "HelveticaNeue-Bold", size: 16)!, backgroundColor: UIColor(hex: 0xf7f7f7), target:self)
      
      
      
      reloadTableView()
      initNotifications()
      
   }
   
   
   
   func initNotifications() {
      Notifier.addObserver(self, event: .jabberReady)
      Notifier.addObserver(self, event: .serverStatusStringChanged)
   }
   
   deinit {
      Notifier.removeObserver(self, event: .jabberReady)
      Notifier.removeObserver(self, event: .serverStatusStringChanged)
   }
   
   func jabberReady(_ notif : Notification) {
      tableView.reloadData()
   }
   func serverStatusStringChanged(_ notif : Notification) {
      parentVC.onlineIndicatorUpdate()
      reloadTableView()
   }
   
   
   func reloadTableView() {
      tableViewArray.removeAll()
      for key in appdata.servers.allKeys {
         tableViewArray.append(ServerView(server:key as! String))
      }
      tableView.reloadData()
   }
   
   
   
}


extension ServersVC: UITableViewDelegate, UITableViewDataSource {
   
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return tableViewArray.count
   }
   
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ServerCell.self)) as! ServerCell
      cell.view = tableViewArray[indexPath.row]
      cell.addSubview(cell.view)
      if cell.view.server == xmppclient.model.server {
         cell.view.checkmarkView.isHidden = false
      } else {
         cell.view.checkmarkView.isHidden = true
      }
      return cell
   }
   
   func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
      let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ServerCell.self)) as! ServerCell
      cell.view.removeFromSuperview()
   }
   
   
   func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
      return tableViewArray[indexPath.row].frame.size.height
   }
   
   
   func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      xmppclient.disconnectXMPP()
      parentVC.onlineIndicatorUpdate()
      xmppclient.connectXMPP(tableViewArray[indexPath.row].server, tls: false)
   }
   
   
   func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
      if (editingStyle == UITableViewCellEditingStyle.delete) {
         tableView.beginUpdates()
         xmppclient.sender.unsubscribePresence(tableViewArray[indexPath.row].server)
         appdata.servers.removeObject(forKey: tableViewArray[indexPath.row].server)
         tableViewArray.remove(at: indexPath.row)
         tableView.deleteRows(at: [indexPath], with: .fade)
         tableView.endUpdates()
      }
   }
   
}


extension ServersVC: UITextFieldDelegate {
   func textFieldShouldReturn(_ textField: UITextField) -> Bool {
      textField.resignFirstResponder()
      if (textField.text == "") {
         return false
      }
      
      let text = textField.text!
      print("textFieldShouldReturn - connect to server: \(text)")
      
      appdata.servers[text] = "testing..."
      reloadTableView()

      textField.text = ""
      
      return true
   }
   
}

