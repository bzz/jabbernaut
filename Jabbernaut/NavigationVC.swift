//black 0x0b0b0b
//blue  0x406894


import UIKit



class NavigationVC : UIViewController
{
   
   
   convenience init() {
      self.init(buttonIndex:4)
   }
   
   init(buttonIndex : Int) {
      super.init(nibName: nil, bundle: nil)
      currentButtonTag = buttonIndex
      self.startVC = vcForButtobIndex(buttonIndex)
   }
   
   
   
   required init?(coder aDecoder: NSCoder) {
      super.init(coder: aDecoder)
   }
   
   
   
   let btnHeight :CGFloat = 49 * UIScreen.main.bounds.width / 320
   let btnWidth  :CGFloat = 64 * UIScreen.main.bounds.width / 320
   
   var topPanel:UIView!
   var addButton:UIButton!
   var titleButton:UIButton!
   var newMessageIndicator:UIView!
   var onlineIndicator:UIImageView!
   
   
   
   
   
   var bottomPanel:UIView!
   var currentButtonTag = 0
   
   var mainViewBed = UIView()
   var currentVC:UIViewController!
   
   
   var startVC : UIViewController?
   
   
   override func viewDidLoad() {
      
      
      let background = UIImageView(frame:view.bounds)
      background.image = UIImage(named:"background");
      view.addSubview(background)
      
      
      
      mainViewBed = UIView(frame: CGRect(x: 0, y: 44+20, width: screenWidth, height: screenHeight-44-btnHeight-20))
      mainViewBed.backgroundColor = UIColor.clear
      view.addSubview(mainViewBed)
      
      
      
      topPanel = UIView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 20+44))
      topPanel.backgroundColor = UIColor(hex: 0xf6f6f7)
      view.addSubview(topPanel);
      
      addButton = UIButton(frame: CGRect(x: 0, y: 20, width: 44, height: 44))
      titleButton = UIButton(frame: CGRect(x: 44, y: 20, width: screenWidth-88, height: 44))
      
      
      onlineIndicator = UIImageView(frame: CGRect(x: screenWidth-44, y: 0, width: 44, height: 63))
      topPanel.addSubview(onlineIndicator)
      onlineIndicatorUpdate()
      
      
      
      bottomPanel = UIView(frame: CGRect(x: 0, y: screenHeight-btnHeight, width: screenWidth, height: btnHeight))
      bottomPanel.backgroundColor = UIColor(hex: 0xffffff)
      view.addSubview(bottomPanel);
      
      newMessageIndicator = UIView(frame: CGRect(x: btnWidth * 2 - 40, y: 17, width: 22, height: 22))
      newMessageIndicator.layer.cornerRadius = onlineIndicator.frame.size.width/2
      newMessageIndicator.backgroundColor = UIColor.clear
      bottomPanel.addSubview(newMessageIndicator)
      
      
      addPanelButton(0, image:"icon_news", imageSelected: "icon_news_active")
      addPanelButton(1, image:"icon_messages", imageSelected: "icon_messages_active")
      addPanelButton(2, image:"icon_friends", imageSelected: "icon_friends_active")
      addPanelButton(3, image:"icon_servers", imageSelected: "icon_servers_active")
      addPanelButton(4, image:"icon_profile", imageSelected: "icon_profile_active")
      
      if (startVC != nil) {
         let toVC = startVC!
         self.addChildViewController(toVC)
         self.mainViewBed.addSubview(toVC.view)
         toVC.didMove(toParentViewController: self)
         self.currentVC = toVC
      }
      
      
      
      initNotifications()
   }
   
   
   func initNotifications() {
      Notifier.addObserver(self, event: .networkOff)
      Notifier.addObserver(self, event: .authSucceed)
      Notifier.addObserver(self, event: .boundResource)
      Notifier.addObserver(self, event: .jabberReady)
      Notifier.addObserver(self, event: .gotMessage)
   }
   deinit {
      Notifier.removeObserver(self, event: .networkOff)
      Notifier.removeObserver(self, event: .authSucceed)
      Notifier.removeObserver(self, event: .boundResource)
      Notifier.removeObserver(self, event: .jabberReady)
      Notifier.removeObserver(self, event: .gotMessage)
   }
   
   
   func networkOff(_ notif : Notification) {
      onlineIndicatorUpdate()
   }
   func authSucceed(_ notif : Notification) {
      onlineIndicatorUpdate()
   }
   func boundResource(_ notif : Notification) {
      onlineIndicatorUpdate()
   }
   func jabberReady(_ notif : Notification) {
      onlineIndicatorUpdate()
   }
   func gotMessage(_ notif : Notification) {
      if let body = notif.userInfo?["body"] as? String {
         if body == "" { return }
         newMessageIndicator.backgroundColor = UIColor.red
      }
   }
   
   
   
   
   func addPanelButton(_ tag:NSInteger, image:String, imageSelected:String) {
      let button = UIButton(frame: CGRect(x: btnWidth*CGFloat(tag), y: 0, width: btnWidth, height: btnHeight))
      button.setBackgroundImage(UIImage(named: image), for: UIControlState())
      button.setBackgroundImage(UIImage(named: imageSelected), for: .selected)
      button.adjustsImageWhenHighlighted = false
      button.addTarget(self, action:#selector(NavigationVC.panelButtonPressed(_:)), for: UIControlEvents.touchDown)
      button.tag = tag
      if tag == currentButtonTag {
         button.isSelected = true
      }
      bottomPanel.addSubview(button)
   }
   
   func panelButtonPressed(_ button:UIButton) {
      for v:UIView in bottomPanel.subviews {
         if v.isKind(of: UIButton.self) {
            let b = v as! UIButton
            b.isSelected = false
         }
      }
      button.isSelected = true;
      
      
      let toVC = vcForButtobIndex(button.tag)
      if currentVC.classForCoder == toVC.classForCoder {return}
      
      
      self.currentVC!.willMove(toParentViewController: nil)
      self.currentVC!.removeFromParentViewController()
      self.addChildViewController(toVC)
      self.mainViewBed.addSubview(toVC.view)
      
      
      let bedCenter = mainViewBed.convert(mainViewBed.center, from: view)
      
      let completion = { (value: Bool) in
         self.currentVC!.view.removeFromSuperview()
         self.currentVC = toVC
         self.bottomPanel.isUserInteractionEnabled = true
      }
      
      self.bottomPanel.isUserInteractionEnabled = false
      toVC.view.center.y = bedCenter.y
      
      if self.currentButtonTag < button.tag {
         toVC.view.center.x = screenWidth + bedCenter.x
         UIView.animate(withDuration: 0.25, animations: {
            self.currentVC?.view.center.x = -screenWidth + (self.currentVC?.view.center.x)!
            toVC.view.center = bedCenter
            }, completion: completion)
      } else {
         toVC.view.center.x = -bedCenter.x
         UIView.animate(withDuration: 0.25, animations: {
            self.currentVC?.view.center.x = screenWidth + (self.currentVC?.view.center.x)!
            toVC.view.center = bedCenter
            }, completion: completion)
      }
      
      
      
      toVC.didMove(toParentViewController: self)
      self.currentButtonTag = button.tag
      onlineIndicatorUpdate()
   }
   
   func vcForButtobIndex(_ buttonIndex : Int) -> UIViewController {
      var toVC = UIViewController()
      switch buttonIndex {
      case 0:
         toVC = NewsVC();
      case 1:
         toVC = ConversationsVC()
      case 2:
         toVC = FriendsVC();
      case 3:
         toVC = ServersVC();
      case 4:
         toVC = ProfileVC();
         
      default: break }
      
      return toVC
   }
   
   
   
   func extraNavigate(_ fromVC: UIViewController?, toVC: UIViewController?) -> Bool {
      if fromVC == toVC {return false}
      
      if (fromVC != nil) {
         fromVC!.willMove(toParentViewController: nil)
         fromVC!.view.removeFromSuperview()
         fromVC!.removeFromParentViewController()
      }
      if (toVC != nil) {
         self.addChildViewController(toVC!)
         mainViewBed.addSubview(toVC!.view)
         toVC!.didMove(toParentViewController: self)
         currentVC = toVC
      }
      return true
   }
   
   
   
   override var preferredStatusBarStyle : UIStatusBarStyle {
      return UIStatusBarStyle.default
   }
   
   
   func setupAddButton(_ imageName:String, target: AnyObject?, action: Selector) {
      addButton.removeTarget(nil, action: nil, for: .allEvents)
      addButton.setBackgroundImage(UIImage(named: imageName), for: UIControlState())
      addButton.addTarget(target, action:action, for: UIControlEvents.touchUpInside)
      topPanel.addSubview(addButton)
   }
   func removeAddButton() {
      addButton.removeTarget(nil, action: nil, for: .allEvents)
      addButton.setBackgroundImage(nil, for: UIControlState())
   }
   
   func setupTitleButton(_ title:String, titleColor:UIColor, font:UIFont, backgroundColor:UIColor, target: AnyObject?) {
      titleButton.setTitle(title, for: UIControlState())
      titleButton.setTitleColor(titleColor, for: UIControlState())
      titleButton.titleLabel?.font = font
      titleButton.backgroundColor = backgroundColor
//      titleButton.addTarget(target, action:action, for: UIControlEvents.touchUpInside)
      topPanel.addSubview(titleButton)
   }
   
   
   func onlineIndicatorUpdate() {
      guard xmppclient.authmodel != nil else { return }
      if xmppclient.authmodel.jabber_authorised {
         if xmppclient.authmodel.jabber_bound_resource {
            onlineIndicator.image = UIImage(named: "icon_lamp_on")
         } else {
            onlineIndicator.image = UIImage(named: "icon_lamp_mid")
         }
      } else {
         onlineIndicator.image = UIImage(named: "icon_lamp_off")
      }
   }
   
}

