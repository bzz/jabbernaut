import UIKit


class FriendCell: UITableViewCell {
   var view : FriendView!
   
   override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
      super.init(style: style, reuseIdentifier: reuseIdentifier)
      self.backgroundColor = UIColor.clear
      self.selectionStyle = .none
   }
   
   required init?(coder aDecoder: NSCoder) {
      super.init(coder: aDecoder)
   }
}


class FriendView: UIView {
   
   var onlineIndicator : UIView!
   var iconView : UIImageView!
   var titleLabel : UILabel!
   var secondLabel : UILabel!
   
   var jid : String!
   
   init(key : String) {
      super.init(frame: CGRect(x:0, y:0, width:UIScreen.main.bounds.width, height:80))
      
      let buddy = Buddy(key: key)
      self.jid = buddy.last_jid
      
      self.backgroundColor = UIColor(hex: 0xffffff)
      
      
      onlineIndicator = UIView(frame:CGRect(x:13, y:13, width:45, height:45))
      onlineIndicator.layer.cornerRadius = onlineIndicator.frame.size.width/2
      if xmppclient.model.isOnline(buddy.last_jid) {
         onlineIndicator.backgroundColor = UIColor(hex: 0x7ce055)
      } else {
         onlineIndicator.backgroundColor = UIColor(hex: 0xa9a9a9)
      }
      self.addSubview(onlineIndicator)

      iconView = UIImageView(frame:CGRect(x:16, y:16, width:39, height:39))
      iconView.backgroundColor = UIColor(hex: 0xf3f3f3)
      iconView.layer.cornerRadius = iconView.frame.width/2.0
      iconView.layer.masksToBounds = true
      if let avatar_string = appdata.avatars[appdata.buddiesKeyForJID(jid)] as? String {
         let img_data = Data.init(base64Encoded: avatar_string, options: .ignoreUnknownCharacters)
         iconView.image = UIImage(data: img_data!)
      }
      self.addSubview(iconView)
      
      
      titleLabel = UILabel(frame:CGRect(x:65, y:7, width:UIScreen.main.bounds.width - 85, height:24))
      titleLabel.textColor = UIColor(hex: 0x000000)
      titleLabel.font = UIFont(name: "HelveticaNeue-Bold", size: 13)
      titleLabel.text = buddy.screen_name
      self.addSubview(titleLabel)
      
      secondLabel = UILabel(frame:CGRect(x:65, y:30, width:UIScreen.main.bounds.width - 85, height:24))
      secondLabel.textColor = UIColor(hex: 0xa9a9a9)
      secondLabel.font = UIFont(name: "HelveticaNeue", size: 11)
      if buddy.address == "" {
         secondLabel.text = buddy.last_jid
      } else {
         secondLabel.text = buddy.address
      }
      self.addSubview(secondLabel)
      
      
      
      self.frame = CGRect(x:0, y:0, width:UIScreen.main.bounds.width, height:secondLabel.frame.height + 52)
      
      
      let separator = UIView(frame: CGRect(x:0, y:self.frame.height-1, width:UIScreen.main.bounds.width, height:1))
      separator.backgroundColor = UIColor(hex: 0xd1d0d4)
      self.addSubview(separator)
   }
   
   required init?(coder aDecoder: NSCoder) {
      super.init(coder: aDecoder)
   }
}

