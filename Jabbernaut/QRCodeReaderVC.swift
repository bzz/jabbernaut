import UIKit
import AVFoundation


protocol QRCodeReaderDelegate {
   
   func gotQRCode(code: String)
}



class QRCodeReaderVC: UIViewController, AVCaptureMetadataOutputObjectsDelegate {
   
   var delegate: QRCodeReaderDelegate?
   var captureSession: AVCaptureSession!
   var code: String?
   
   
   override func viewDidLoad() {
      super.viewDidLoad()
      
      self.captureSession = AVCaptureSession();
      let videoCaptureDevice: AVCaptureDevice = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
      
      do {
         let videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
         
         if self.captureSession.canAddInput(videoInput) {
            self.captureSession.addInput(videoInput)
         } else {
            print("Could not add video input")
         }
         
         let metadataOutput = AVCaptureMetadataOutput()
         if self.captureSession.canAddOutput(metadataOutput) {
            self.captureSession.addOutput(metadataOutput)
            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = [AVMetadataObjectTypeQRCode]
            //AVMetadataObjectTypeQRCode,AVMetadataObjectTypeEAN8Code, AVMetadataObjectTypeEAN13Code, AVMetadataObjectTypePDF417Code
         } else {
            print("Could not add metadata output")
         }
         let previewLayer = AVCaptureVideoPreviewLayer(session: self.captureSession)
         previewLayer?.frame = self.view.layer.bounds
         self.view.layer.addSublayer(previewLayer!)
         self.captureSession.startRunning()
      } catch let error as NSError {
         print("Error while creating vide input device: \(error.localizedDescription)")
      }
      
      
      
   }
   
   func captureOutput(_ captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [Any]!, from connection: AVCaptureConnection!) {
      print("caught QR code")
      for metadata in metadataObjects {
         let readableObject = metadata as! AVMetadataMachineReadableCodeObject
         let code = readableObject.stringValue
         if  code!.isEmpty {
            print("is empty")
         } else {
            self.captureSession.stopRunning()
            self.dismiss(animated: true, completion: nil)
            self.delegate?.gotQRCode(code: code!)
         }
      }
   }
}
