import UIKit


class TagField: UIView {
   var label = UILabel()
   var textField = UITextField()
   var text : String {
      get {
         return textField.text!
      }
      set {
         textField.text = newValue
      }
   }
   var placeholder : String {
      get {
         return textField.placeholder!
      }
      set {
         textField.placeholder = newValue
      }
   }
   
   
   required init?(coder aDecoder: NSCoder) {
      super.init(coder: aDecoder)
   }
   
   override init(frame: CGRect) {
      super.init(frame: frame)
      
      self.backgroundColor = UIColor(hex: 0xf7f7f7)

      label = UILabel(frame: CGRect(x: 0, y: 0, width: 50, height: 16))
      label.backgroundColor = UIColor(hex: 0xf72222)
      label.textColor = UIColor(hex: 0xffffff)
      label.font = UIFont(name: "Courier-Bold", size: 8)
      self.addSubview(label)
      
      let separator = UIView(frame: CGRect(x:0, y:0, width:UIScreen.main.bounds.width, height:1))
      separator.backgroundColor = UIColor(hex: 0xd1d0d4)
      self.addSubview(separator)
      
      textField = UITextField(frame: CGRect(x: 60, y: 0, width: frame.size.width-60, height: 30))
//      textField.addTarget(self, action: #selector(textFieldEditingFinished(_:)), forControlEvents: UIControlEvents.EditingDidEndOnExit)
      textField.textAlignment = .left
      textField.textColor = UIColor(hex: 0x000000)
      textField.font = UIFont(name: "HelveticaNeue-Thin", size: 12)
      self.addSubview(textField)
      

   }
   
   
   
}
