import UIKit
import UserNotifications


let screenHeight = UIScreen.main.bounds.height
let screenWidth = UIScreen.main.bounds.width
let SEPARATOR = "::"



let appDelegate = UIApplication.shared.delegate as! AppDelegate


var xmppclient = XMPPClient()
let btcmanager = BTCManager()
let appdata = AppData()



@UIApplicationMain
open class AppDelegate: UIResponder, UIApplicationDelegate {
   
   open var window: UIWindow?
   var settings: UIUserNotificationSettings?
   
   
   open func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

      if appdata.secretHEXString == "" {
         appdata.randomizeSecretHEXString()
      } else {
         let data = appdata.secretHEXString.dataFromHexadecimalString()
         appdata.addressString = btcmanager.address(fromSecretKey: data!)
      }
      
      window = UIWindow(frame: UIScreen.main.bounds)
      window!.backgroundColor = UIColor.white
      window!.rootViewController = WelcomeVC()
      window!.makeKeyAndVisible()
      
      if #available(iOS 10.0, *) {
         let center = UNUserNotificationCenter.current()
         center.requestAuthorization(options:[.badge, .alert, .sound]) { (granted, error) in
            // Enable or disable features based on authorization.
         }
      } else {
         // Fallback on earlier versions
         let settings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
         application.registerUserNotificationSettings(settings)
         
      }

      application.registerForRemoteNotifications()

      return true
   }
   
   
   
   open func proceedToNavigation() {
      DispatchQueue.main.async(execute: {
         self.window!.rootViewController = NavigationVC(buttonIndex:3)
         self.window!.makeKeyAndVisible()
      })
      
      appdata.loadAll()
      
      appdata.servers["xmppcomm.net"] = "valid server";
      appdata.servers["xmppcomm.com"] = "valid server";
      appdata.servers["jsmart.web.id"] = "valid server";
      appdata.servers["null.pm"] = "valid server";
      
      xmppclient.connectXMPP("xmppcomm.com", tls: false)
   }
   
   

   
   public func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
      let token = String(format: "%@", deviceToken as CVarArg).trimmingCharacters(in: CharacterSet(charactersIn: "<>")).replacingOccurrences(of: " ", with: "")
      print(token)
      appdata.APNSToken = token      
   }
   
   
   public func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
      print("didReceiveRemoteNotification")
      print(userInfo.description)
   }
 
   public func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
      print("didReceiveRemoteNotification fetchCompletionHandler")
      print(userInfo.description)
   }
   
   
   
   open func applicationWillResignActive(_ application: UIApplication) {
      appdata.saveAll()
      
      if xmppclient.sender != nil {
         xmppclient.sender.showPresenceAway()
      }
   }
   
   open func applicationWillEnterForeground(_ application: UIApplication) {
      guard xmppclient.authmodel != nil else { return }
      if xmppclient.authmodel.jabber_ready {
         xmppclient.sender.showPresence()
      }
   }
   
   
   open func alert(_ title:String, message: String) {
      let alert = UIAlertController(title: title, message:message, preferredStyle: .alert)
      alert.addAction(UIAlertAction(title: "OK", style: .default) { _ in })
      self.window?.rootViewController!.present(alert, animated: true){}
   }
   
   
   
}

