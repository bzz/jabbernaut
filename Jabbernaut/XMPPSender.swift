import UIKit


class XMPPSender: NSObject {
   var textQueue = [String]()
   

   
   
   func enqueue(_ text : String!) {
      if text == nil {
         return
      }
      textQueue.insert(text, at:0)
      send()
   }
   
   func send() {
      if textQueue.count == 0 {
         return
      }
      if let last = textQueue.last {
         xmppclient.jabberconnector.send(last)
         textQueue.removeLast()
      }
   }
   
   

   
   
//   func sendHeadline(subject:String, body:String, toJID:String) {
//      enqueue("<message from='\(xmppclient.model.jid_full)' to='\(toJID)' type='headline'><subject>\(subject)</subject><body>\(body)</body><delay xmlns='urn:xmpp:delay' from='\(xmppclient.model.server)' stamp='2016-03-17T12:35:54.048Z'>Offline Storage</delay></message><presence from='\(xmppclient.model.jid_full)' to='\(toJID)'><show/></presence>")
//   }
   
//   func sendMessage(body:String, toJID:String) {
//      enqueue("<message from='\(xmppclient.model.jid_full)' to='\(toJID)'><body>\(body)</body></message><presence from='\(xmppclient.model.jid_full)' to='\(toJID)'><show/></presence>")      
//   }
   func sendMessage(_ body:String, toJID:String, messageID:String) {
      enqueue("<message from='\(xmppclient.model.jid_full)' to='\(toJID)' id='\(messageID)'><body>\(body)</body><request xmlns='urn:xmpp:receipts'/></message>")
   }
   
   
   
   
   func confirmMessageReceipt(_ toJID:String, messageID:String) {
      enqueue("<message from='\(xmppclient.model.jid_full)' to='\(toJID)' id='\(messageID)'><received xmlns='urn:xmpp:receipts' id='\(messageID)'/></message>")
   }
   
   
   
   func sendVCARD() {      
      let buddy = Buddy(string: appdata.userInfoString)
      enqueue(buddy.vCard)
   }
   
   func getVCARD(_ jid:String) {
      enqueue("<iq id='\(String.randomStringWithLength(8))' to='\(jid)' type='get'><vCard xmlns='vcard-temp'/></iq>")
   }
   
   
   func getRoster() {
      enqueue("<iq from='\(xmppclient.model.jid_full)' type='get'><query xmlns='jabber:iq:roster'/></iq>")
      
   }
   
   
   
   
   func requestSubscription(_ jid : String) {
      print("REQUESTING SUBASCRIPTION")
//      enqueue("<presence from='\(xmppclient.model.jid)' to='\(jid)' type='subscribe'/>")
      enqueue("<presence from='\(xmppclient.model.jid_full)' to='\(jid)' type='subscribe'><status></status></presence><presence><show/></presence>") //
   }
   func subscribePresence(_ jid : String) {
      enqueue("<presence to='\(jid)' type='subscribed'/>")
   }
   func unsubscribePresence(_ jid : String) {
      enqueue("<presence to='\(jid)' type='unsubscribed'/>")
   }
 
   
   
   
   
   
   func showPresence() {
      enqueue("<presence><show/></presence>")
   }
   func showPresenceAway() {
      enqueue("<presence><show>away</show></presence> ")
   }
   
   
   
   
   
   
   func ping(_ jid:String) {
      enqueue("<iq from='\(xmppclient.model.jid_full)' to='\(jid)' id='PING' type='get'><ping xmlns='urn:xmpp:ping'/></iq>")
   }
   func pong(_ jid:String) {
      enqueue("<iq from='\(xmppclient.model.jid_full)' to='\(jid)' id='PING' type='result'/>")
   }
   

   
   
   
   //TODO: untested PLAIN auth
   func authenticatePLAIN(_ login : String, password : String) {
      
      guard let l = login.data(using: String.Encoding.utf8) else { return }
      guard let p = password.data(using: String.Encoding.utf8) else { return }
      let zero = NSMutableData(length: 1)!
      let data = (l as NSData).mutableCopy() as! NSMutableData
      data.append(zero as Data)
      data.append(p as Data)
      data.append(zero as Data)
      let str = (data as AnyObject).base64EncodedString(options: [])
      
      enqueue("<auth xmlns='urn:ietf:params:xml:ns:xmpp-sasl' mechanism='PLAIN'>\(str)=</auth>")
      
   }
   
   
   func authenticate() {
      if xmppclient.authmodel.DIGEST_MD5_AUTH_AVAILABLE {
         enqueue("<auth xmlns='urn:ietf:params:xml:ns:xmpp-sasl' mechanism='DIGEST-MD5'/>")
         return
      }
      if xmppclient.authmodel.PLAIN_AUTH_AVAILABLE {
         xmppclient.sender.authenticatePLAIN(xmppclient.model.login, password: xmppclient.model.password)
         return
      }
      if xmppclient.authmodel.SCRAM_SHA1_AUTH_AVAILABLE {
         let clientNonce = String.randomStringWithLength(32)
         let str = "n,,n=\(xmppclient.model.login),r=\(clientNonce)"
         enqueue("<auth xmlns='urn:ietf:params:xml:ns:xmpp-sasl' mechanism='SCRAM-SHA-1'>\(str.toBase64())</auth>")
         return
      }
   }
   
   
   func pubsubPublish(_ node : String, title : String, text : String) {
      enqueue("<iq type='set' from='\(xmppclient.model.jid_full)' to='pubsub.\(xmppclient.model.server)' id='pub1'><pubsub xmlns='http://jabber.org/protocol/pubsub'> <publish node='\(node)'><item><entry xmlns='http://www.w3.org/2005/Atom'><title>TITLE</title><summary>To be, or not to be</summary><link rel='alternate' type='text/html' href='http://denmark.lit/2003/12/13/atom03'/><id>tag:denmark.lit,2003:entry-32397</id><published>2003-12-13T18:30:02Z</published><updated>2003-12-13T18:30:02Z</updated></entry></item></publish></pubsub></iq>")
   }
   
   func pubsubDelete(_ node : String, nodeID : String) {
      enqueue("<iq type='set' from='\(xmppclient.model.jid_full)' to='pubsub.\(xmppclient.model.server)' id='DELETE_NODE'><pubsub xmlns='http://jabber.org/protocol/pubsub'><retract node='\(node)'><item id='\(nodeID)'/></retract></pubsub></iq>")
   }
   
   

   func pubsubReadAll() {
      enqueue("<iq type='get' from='\(xmppclient.model.jid_full)' to='pubsub.\(xmppclient.model.server)' id='nodes1'><query xmlns='http://jabber.org/protocol/disco#items'/></iq>")
   }
   
   func pubsubReadNode(_ node : String) {
      enqueue("<iq type='get' from='\(xmppclient.model.jid_full)' to='pubsub.\(xmppclient.model.server)' id='\(String.randomStringWithLength(10))'><query xmlns='http://jabber.org/protocol/disco#items' node='\(node)'/></iq>")
   }

}







//Authmodel
extension XMPPSender {
   
   func send_registration_info() {
      enqueue("<iq type='set' id='reg2'><query xmlns='jabber:iq:register'><username>\(xmppclient.model.login)</username><password>\(xmppclient.model.password)</password></query></iq>")
   }
   
   func getSession() {
      enqueue("<?xml version='1.0'?><stream:stream xmlns:stream='http://etherx.jabber.org/streams' xmlns='jabber:client' version='1.0' from='\(xmppclient.model.login)@\(xmppclient.model.server)' to='\(xmppclient.model.server)'>" )
   }
   
   
   
   func bindResource(_ resource: String) {
      enqueue("<iq id='BIND' type='set'><bind xmlns='urn:ietf:params:xml:ns:xmpp-bind'><resource>\(resource)</resource></bind></iq>")
   }
   
   func startSession() {
      let session_id = String.randomStringWithLength(8)
      enqueue("<iq to='\(xmppclient.model.server)' type='set' id='\(session_id)'><session xmlns='urn:ietf:params:xml:ns:xmpp-session'/></iq>")
      
   }
   
   
   
   
   func auth_solve_challenge_DIGEST_MD5(realm:String, nonce:String, qop:String, charset:String, algorithm:String) {
      print("AUTHORISING USING DIGEST-MD5")
      
      let auth = XMPPDigestMD5Authentication()
      auth.username = xmppclient.model.login
      auth.password = xmppclient.model.password
    if let out = auth.base64EncodedFullResponse(withRealm: realm, nonce:nonce, cnonce:String.randomNumberWithLength(19), qop:qop, digestURI:"xmpp/\(xmppclient.model.server)") {
        enqueue("<response xmlns='urn:ietf:params:xml:ns:xmpp-sasl'>\(out)</response>")
    }
      
   }
   
   func auth_DIGEST_MD5_solve_final_challenge() {
      enqueue("<response xmlns='urn:ietf:params:xml:ns:xmpp-sasl'/>")
   }
   
   
   
   func auth_solve_challenge_SCRAM_SHA1(combinedNonce:String, salt:String, countStr:String) {
      print("AUTHORISING USING SCRAM-SHA-1")
      //TODO: SCRAM AUTH NOT IMPLEMENTED
      let auth = XMPPDigestMD5Authentication()
      auth.username = xmppclient.model.login
      auth.password = xmppclient.model.password
      let out : String = auth.handleAuth1(withCombinedNonce: combinedNonce, salt:salt, countStr:countStr)
      
      //      let clientFinalMessageBare = "c=biws,r=" + combinedNonce
      //      var s : NSMutableString = "                                                  c       "
      //      var out = UnsafeMutablePointer<UInt8>(s.UTF8String)
      //      PKCS5_PBKDF2_HMAC_SHA1(password, Int32(password.characters.count), salt, Int32(salt.characters.count), Int32(count)!, Int32(salt.characters.count), out)
      //      var saltedPassword = UnsafeMutablePointer<Int8>(out)
      //      print(String.fromCString(saltedPassword))
      
      /*
       clientFinalMessageBare = "c=biws,r=" .. serverNonce
       saltedPassword = PBKDF2-SHA-1(normalizedPassword, salt, i)
       clientKey = HMAC-SHA-1(saltedPassword, "Client Key")
       storedKey = SHA-1(clientKey)
       authMessage = initialMessage .. "," .. serverFirstMessage .. "," .. clientFinalMessageBare
       clientSignature = HMAC-SHA-1(storedKey, authMessage)
       clientProof = clientKey XOR clientSignature
       serverKey = HMAC-SHA-1(saltedPassword, "Server Key")
       serverSignature = HMAC-SHA-1(serverKey, authMessage)
       clientFinalMessage = clientFinalMessageBare .. ",p=" .. base64(clientProof)
       */
      
      enqueue("<response xmlns='urn:ietf:params:xml:ns:xmpp-sasl'>\(out)</response>")
   }
   
}





