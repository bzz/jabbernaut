import Foundation



enum SocketState : Int {
   case Init = 0
   case connect = 1
   case disconnect = 2
   case errorOccurred = 3
   
}


class JabberConnector: NSObject {
   
   
   private var writeData = Data()
   private var totalBytesWritten = 0
   var outputStream : OutputStream!
   var outputDelegate: StreamDelegate!

   
   private var readData = Data()
   private var totalBytesRead = 0
   var inputStream  : InputStream!
   var inputDelegate: StreamDelegate!

   
   
   //   var runloop      : RunLoop?
   var status : Int = -1
   //   var timeout      : Float = 5.0;
   let JABBER_PORT : UInt32 = 5222
   var use_tls = false
   
   var xmlreader : XMLReader!
   
   override init() {
      super.init()
      xmlreader = XMLReader.shared()
   }
   
   
   func connect(_ server : String) -> Bool {
      print("connect server: ", server)
      //TODO:      DispatchQueue.global().async {
      xmppclient.sender.enqueue("<?xml version='1.0'?><stream:stream xmlns:stream='http://etherx.jabber.org/streams' xmlns='jabber:client' to='\(xmppclient.model.server)' version='1.0'>")
      
      var readstream : Unmanaged<CFReadStream>?
      var writestream : Unmanaged<CFWriteStream>?
      
      CFStreamCreatePairWithSocketToHost(kCFAllocatorDefault, server as CFString, JABBER_PORT, &readstream, &writestream)
      
      self.inputStream = readstream!.takeRetainedValue()
      self.outputStream = writestream!.takeRetainedValue()
      
      self.openStreams()
      //      }
      return true
   }
   
   func connectTLS(_ server : String) -> Bool {
      //
      return true
   }

   
   
   
   func openStreams() {
      inputDelegate = self
      outputDelegate = self
      
      self.inputStream.delegate = inputDelegate
      self.outputStream.delegate = outputDelegate
      
      self.inputStream.schedule(in: RunLoop.current, forMode: RunLoopMode.defaultRunLoopMode)
      self.outputStream.schedule(in: RunLoop.current, forMode: RunLoopMode.defaultRunLoopMode)
      
      self.inputStream.open()
      self.outputStream.open()
   }
   
   
   
   
   public func send(_ message : String) {
      guard outputStream != nil else {
         print("outputStream == nil, send failed")
         DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .seconds(2)) {
            self.send(message)
         }
         return
      }
      print("SEND  " + message)
      guard let data = message.data(using: String.Encoding.utf8) else {
         print("Failed to convert message to Data")
         return
      }
      writeData = data
      
      print("writeData.count=", writeData.count)

      let bytesLeft = self.writeData.count// - self.totalBytesWritten
      print("bytesleft=",bytesLeft)
      
      
      let maxBufferSize = 8192
      let bufferSize = (bytesLeft >= maxBufferSize) ? maxBufferSize : bytesLeft
      print("bufferSize=", bufferSize)
      print("self.totalBytesWritten=", self.totalBytesWritten)
      
      
      
      let pointer = UnsafeMutablePointer<UInt8>.allocate(capacity: writeData.count)
//      UnsafeMutableBufferPointer.initialize(from: writeData)
      pointer.initialize(from: writeData)
      
//      let startBytePointer = pointer.advanced(by: self.totalBytesWritten)
      let buffer = [UInt8](repeating: 0, count: bufferSize)
      let dst = UnsafeMutableRawPointer(mutating: buffer)
      let src = UnsafeMutableRawPointer(pointer)
      memcpy(dst, src, bufferSize)
      let result = self.outputStream.write(buffer, maxLength: bufferSize)
      
      
         if result > 0 {
            let bytesWritten = result
            print("Wrote \(bytesWritten) bytes out")
            self.totalBytesWritten += bytesWritten
//            outputStream.close()
         } else if result == 0 {
            print("Reached the end of the buffer while writing")
//            closeStream(outputStream)
//            self.totalBytesWritten = 0
         } else {
            print("Output stream read error: \(self.outputStream.streamError?.localizedDescription ?? "")")
//            outputStream.close()
         }
//      })
   }
   
   
   public func disconnect() {
      inputStream.close()
      inputStream.remove(from: RunLoop.current, forMode: RunLoopMode.defaultRunLoopMode)
      outputStream.close()
      outputStream.remove(from: RunLoop.current, forMode: RunLoopMode.defaultRunLoopMode)
   }
   
}


extension JabberConnector : StreamDelegate {
   func stream(_ aStream: Stream, handle eventCode: Stream.Event) {
      
      
      
      if aStream.isKind(of: OutputStream.self) {
         
         switch (eventCode) {
         case Stream.Event.openCompleted:
            print("-OutputStream openCompleted-")
            self.status = SocketState.connect.rawValue
            break
            
         case Stream.Event.hasSpaceAvailable:
         print("-OutputStream hasSpaceAvailable-")
         
         if use_tls {
//            startTLS()
         } else {
            xmppclient.sender.send()
         }
         break
            
         case Stream.Event.errorOccurred:
            print("-OutputStream errorOccurred-")
            break
            
         case Stream.Event.endEncountered:
            print("-OutputStream endEncountered-")
            self.status = SocketState.errorOccurred.rawValue
//            closeStream(outputStream)
            break

         default:
            print("")
         }
         
         
      } else if aStream.isKind(of: InputStream.self) {
         switch (eventCode) {
            
         case Stream.Event.openCompleted:
            print("-InputStream openCompleted-")
            self.status = SocketState.connect.rawValue
            break
            
         case Stream.Event.hasBytesAvailable:
            print("-InputStream hasBytesAvailable-")
            
            if let inputStream = aStream as? InputStream
            {
               outputStream.open()
               if inputStream.hasBytesAvailable
               {
                  let bufferSize = 8192
                  var buffer = Array<UInt8>(repeating: 0, count: bufferSize)
                  let bytesRead: Int = inputStream.read(&buffer, maxLength: bufferSize)
                  
                  if bytesRead < 0 {
                     print("Stream error")
//                     outputStream.close()
                     return
                  }
                  else if bytesRead == 0 {
//                     outputStream.close()
                  }
                  else if bytesRead > 0 {
                     let response : String = NSString(bytes: &buffer, length: bytesRead, encoding: String.Encoding.utf8.rawValue)! as String
                     print("RECV  " + response)
                     xmlreader.addText(response)
                     if let d =  xmlreader.dictionaryFromText() {
                        xmppclient.parser.root = d as NSDictionary
                        print(d)
            
                        if !xmppclient.authmodel.jabber_authorised {
                           xmppclient.parser.checkInitialServerResponse()
                        }
                        xmppclient.parser.checkServerResponse()
            
                     } else {
                        print("COULDN'T PARSE TO DICTIONARY")
                     }

//                     outputStream.close()
                  }
               }
            }
            break

         case Stream.Event.errorOccurred:
            print("-InputStream errorOccurred-")
            Notifier.post(.networkOff)
            break
            
         case Stream.Event.endEncountered:
            print("-InputStream endEncountered-")
            self.status = SocketState.errorOccurred.rawValue
            break

         default:
            print("")
         }

      }

      //      mStreamDelegate?.stream?(aStream, handle: eventCode)
   }
}

//extension JabberConnector : GCDAsyncSocketDelegate {
//
//   public func socket(_ sock: GCDAsyncSocket, didConnectToHost host: String, port: UInt16) {
//      print("connected \(host)")
//
//      if use_tls {
//         startTLS()
//      } else {
//         xmppclient.sender.enqueue("<?xml version='1.0'?><stream:stream xmlns:stream='http://etherx.jabber.org/streams' xmlns='jabber:client' to='\(xmppclient.model.server)' version='1.0'>")
//      }
//   }
//
//
//   @nonobjc public func socketDidDisconnect(_ sock: GCDAsyncSocket, withError err: NSError?) {
//      print("socketDidDisconnect")
//      if let e = err {
//         print(e.description)
//         use_tls = false
//      }
//      if use_tls {
//         connectTLS(xmppclient.model.server)
//      }
//   }
//
//
//   public func socket(_ sock: GCDAsyncSocket, didRead data: Data, withTag tag: Int) {
//      if let response = NSString(data: data, encoding: String.Encoding.utf8.rawValue) {
//         print("RECV  " + (response as String))
//
//         xmlreader.addText(response as String)
//         if let d =  xmlreader.dictionaryFromText() {
//            xmppclient.parser.root = d as NSDictionary
//            print(d)
//
//            if !xmppclient.authmodel.jabber_authorised {
//               xmppclient.parser.checkInitialServerResponse()
//            }
//            xmppclient.parser.checkServerResponse()
//
//         } else {
//            print("COULDN'T PARSE TO DICTIONARY")
//         }
//      }
////      socket.readData(withTimeout: 10, tag: 0)
//   }
//
//
//   func startTLS() {
//      let settings = [GCDAsyncSocketManuallyEvaluateTrust : true]
//
//      //      (NSString *)kCFStreamSSLValidatesCertificateChain: (id)kCFBooleanFalse
//
//      //[GCDAsyncSocketManuallyEvaluateTrust : NSNumber(bool: true), GCDAsyncSocketSSLProtocolVersionMin : NSNumber(int: 4), GCDAsyncSocketSSLProtocolVersionMax : NSNumber(int: 8), ]
//
////      socket.startTLS(settings as [String : NSObject])
//   }
//
//
//   public func socketDidSecure(_ sock: GCDAsyncSocket) {
//      print("socketDidSecure")
//      xmppclient.sender.enqueue("<stream:stream xmlns='jabber:client' xmlns:stream='http://etherx.jabber.org/streams' to='example.com' version='1.0'>")
////      socket.readData(withTimeout: 10, tag: 0)
//   }
//
//
//   public func socket(_ sock: GCDAsyncSocket, didReceive trust: SecTrust, completionHandler: (Bool) -> Void) {
//      completionHandler(true)
//      print("didReceiveTrust")
//   }
//
//
//
//
//}
//
