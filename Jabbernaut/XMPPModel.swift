import UIKit



open class XMPPModel: NSObject {

   
   var friendsOnline = Set<String>()
   var friendsAway = Set<String>()
   var friendsGotVCARD = Set<String>()
   

   
   var roster  = NSMutableDictionary()  //  [ jid : subscription(both/from/to) ]
   var pubsubnodes  = NSMutableDictionary()  //  [ node : node_name ]


   var password = ""
   let resource = "ios"
   

   var jid = "" {
      didSet {
         xmppclient.model.jid_full = xmppclient.model.jid + "/" + xmppclient.model.resource
      }
   }
   var jid_full = ""
   
   
   var login = "" {
      didSet {
         xmppclient.model.jid = login + "@" + server
         xmppclient.model.jid_full = xmppclient.model.jid + "/" + xmppclient.model.resource
      }
   }
   var server = "" {
      didSet {
         xmppclient.model.jid = login + "@" + server
         xmppclient.model.jid_full = xmppclient.model.jid + "/" + xmppclient.model.resource
      }
   }
   

//   var session_id :String = ""

   
   override init() {
      super.init()
      
      Notifier.addObserver(self, event: .friendOnline)
      Notifier.addObserver(self, event: .friendOffline)
      Notifier.addObserver(self, event: .friendAway)
      Notifier.addObserver(self, event: .gotMessage)
      Notifier.addObserver(self, event: .messageReceiptConfirmed)

      Notifier.addObserver(self, event: .gotPong)
      Notifier.addObserver(self, event: .gotPostsubNodes)
      Notifier.addObserver(self, event: .gotRoster)
      Notifier.addObserver(self, event: .gotPresence)
   }

   
   
   deinit {
      Notifier.removeObserver(self, event: .friendOnline)
      Notifier.removeObserver(self, event: .friendOffline)
      Notifier.removeObserver(self, event: .friendAway)
      Notifier.removeObserver(self, event: .gotMessage)
      Notifier.removeObserver(self, event: .messageReceiptConfirmed)

      Notifier.removeObserver(self, event: .gotPong)
      Notifier.removeObserver(self, event: .gotPostsubNodes)
      Notifier.removeObserver(self, event: .gotRoster)
      Notifier.removeObserver(self, event: .gotPresence)
   }
   

   func isOnline(_ jid:String) -> Bool {
      for f in friendsOnline {
         if f == jid {
            return true
         }
      }
      return false
   }
   
   
   func friendGotVCARD(_ jid:String) -> Bool {
      for f in friendsGotVCARD {
         if f == jid {
            return true
         }
      }
      return false
   }
   
   
   
   
   
   
   func friendOnline(_ notif : Notification) {
      if let jid_online_full = notif.userInfo?["from"] as? String {
         let jid_online = jid_online_full.components(separatedBy: "/")[0]
         if jid_online != xmppclient.model.jid {
            friendsOnline.insert(jid_online)
            friendsAway.remove(jid_online)

//send unsent messages
/*
            if let mail = appDelegate.history[jid_online] as? NSArray {
               for letter in mail {
                  let mess = Message(string: letter as! String)
                  if mess.messageID != "SENT" && mess.from == xmppclient.model.jid {
                     print("SENDING UNSENT MESSAGE TO \(jid): \"\(mess.body)\"")
                     
                     xmppclient.sender.sendMessage(mess.body, toJID: jid_online, messageID: mess.messageID)
                  }
               }
            }
 */
         }
      }
   }

   func friendAway(_ notif : Notification) {
      if let from = notif.userInfo?["from"] as? String {
         let jid = from.components(separatedBy: "/")[0]
         if jid != xmppclient.model.jid {
            friendsOnline.remove(jid)
            friendsAway.insert(jid)
            print("FRIEND AWAY")
            print(from)
         }
      }
   }
   func friendOffline(_ notif : Notification) {
      if let from = notif.userInfo?["from"] as? String {
         let jid = from.components(separatedBy: "/")[0]
         if jid != xmppclient.model.jid {
            friendsOnline.remove(jid)
            friendsAway.remove(jid)
            print("FRIEND OFFLINE")
            print(from)
         }
      }
   }
   
   
   func gotMessage(_ notif : Notification) {
      print("gotMessage")
      if let
         from = notif.userInfo?["from"] as? String,
         let body = notif.userInfo?["body"] as? String
      {
         let from_jid = from.components(separatedBy: "/")[0]
         var messageID = notif.userInfo?["messageID"] as? String
         if messageID == nil {
            messageID = "noID"
         } else {
            print("xmppclient.sender.confirmMessageReceipt   \(messageID)")
            xmppclient.sender.confirmMessageReceipt(from, messageID:messageID!)
         }
         let mess = Message(string: "")
         mess.from = from_jid
         mess.body = body
         mess.messageID = messageID!
         mess.date = Date().UTCString()
         
         
         
         print("appdata.buddies")
         print(appdata.buddies)
         print("appDelegate.history")
         print(appdata.history)
                  
         
         let buddy = Buddy(key: from_jid)
         buddy.last_jid = from_jid
         if buddy.address == "" {
            buddy.name = from_jid
            buddy.save()
            Notifier.post(.newContactAdded, userInfo: ["from": from_jid])
         } else {
            buddy.save()
         }
         
         
         
         let mail = buddy.history()
         
         for letter in mail {
            if Message(string: letter as! String).messageID == mess.messageID {
               return
            }
         }
         mail.add(mess.string)

         buddy.saveHistory(mail)
                  
         
         Notifier.post(.gotMessageShow, userInfo: ["from":from_jid])

      }
   }
   
   
   func messageReceiptConfirmed(_ notif : Notification) {
      guard let got_messageID = notif.userInfo?["messageID"] as? String else { return }
      for key in appdata.history.allKeys {
         let chatHistory = appdata.history.value(forKey: key as! String) as! NSMutableArray
         for i in 0..<chatHistory.count {
            let mess = Message(string: chatHistory[i] as! String)
            if mess.messageID == got_messageID {
               mess.messageID = "SENT"
               chatHistory[i] = mess.string
               appdata.history.setValue(chatHistory, forKey: key as! String)
            }
         }
      }
      Notifier.post(.messageReceiptConfirmedReloadData)
   }


   
   
   func gotPong(_ notif : Notification) {
      if let from = notif.userInfo?["from"] as? String {
         print("GOT PONG FROM \(from)")
      }
   }
   func gotPostsubNodes(_ notif : Notification) {
      for node in pubsubnodes {
         xmppclient.sender.pubsubReadNode(node.key as! String)
      }
      print("POSTSUB NODES : \(pubsubnodes)")
   }


   func gotRoster(_ notif : Notification) {
      print("ROSTER : \(xmppclient.model.roster)")
   }
   
   
   func gotPresence(_ notif : Notification) {
      guard let from_jid = notif.userInfo?["from"] as? String else { return }
      let type = notif.userInfo?["type"] as? String
      let show = notif.userInfo?["show"] as? String
      if type == "subscribe" {
         xmppclient.sender.subscribePresence(from_jid)
         
         if !xmppclient.model.friendGotVCARD(from_jid) {
            //TODO: only get if none of jids for address present in xmppclient.model.friendsGotVCARD
            xmppclient.sender.getVCARD(from_jid)
         }
         
      }
      if appdata.buddies[from_jid] == nil {
         let arr = from_jid.components(separatedBy: "@")
         if arr.count > 1 {
            if appdata.buddiesKeyForJID(from_jid) == from_jid {
               let buddy = Buddy()
               buddy.last_jid = from_jid
               buddy.name = from_jid
               appdata.buddies[from_jid] = buddy.string
               Notifier.post(.newContactAdded, userInfo: ["from": from_jid])
            }
         }
      }
      if type == "unavailable" {
         Notifier.post(.friendOffline, userInfo: ["from": from_jid])
         return
      }
      if type == "" {
         if show == "away" {
            Notifier.post(.friendAway, userInfo: ["from": from_jid])
         } else {
            Notifier.post(.friendOnline, userInfo: ["from": from_jid])
         }
      }
   }
   

}

