import UIKit

class NewsVC: UIViewController {
   
   var feedArray: [UIView] = []
   var tableView = UITableView()
   
   var addButtonMenu = UIView()

/*
   var filterVC:UIViewController!
   var curtain: UIView!
   var animator: UIDynamicAnimator!
   var gravity: UIGravityBehavior!
   var collision: UICollisionBehavior!
*/
   
   
   var parentVC : NavigationVC!
   override func viewDidLoad() {
      parentVC = parent as! NavigationVC
      view.frame = parentVC.mainViewBed.bounds
      
      view.backgroundColor = UIColor(hexA:0xffffff20)
      
      
      tableView = UITableView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height))
      tableView.delegate = self
      tableView.dataSource = self
      tableView.register(FeedCell.self, forCellReuseIdentifier: String(describing: FeedCell.self))
      tableView.backgroundColor = UIColor.clear
      tableView.showsVerticalScrollIndicator = false
      
      tableView.separatorStyle = .none
      tableView.rowHeight = UITableViewAutomaticDimension;
      //        tableView.estimatedRowHeight = 100.0;
      
      
      let headerView = UIView(frame: CGRect(x:0, y:0, width:view.frame.width, height:130))
      
      let headerContentView = UIView(frame: headerView.bounds)
      headerContentView.backgroundColor = UIColor(patternImage: UIImage(named: "feed_header")!)
      headerView.addSubview(headerContentView)
      tableView.tableHeaderView = headerView
      
      
      tableView.contentInset = UIEdgeInsetsMake(10, 0, -10, 0);
      
      
//      feedArray.append(dpiScale(UIImageView(image:UIImage(named: "post1"))))
//      feedArray.append(dpiScale(UIImageView(image: UIImage(named: "post2"))))
      
      parentVC.setupAddButton("envelope", target:self, action:#selector(NewsVC.addButtonPressed))
      parentVC.setupTitleButton("Credits   ", titleColor: UIColor(hex: 0x000000), font: UIFont(name: "HelveticaNeue-Bold", size: 16)!, backgroundColor: UIColor(hex: 0xf7f7f7), target:self)
//, action:#selector(NewsVC.curtainDown)

      view.addSubview(tableView)
//      setupCurtain()

   }
   func dpiScale(_ image:UIView!) -> UIView! {
      let dpi = screenWidth / 320
      image.frame = CGRect(x:image.frame.origin.x, y:image.frame.origin.y, width:image.frame.size.width * dpi, height:image.frame.size.height * dpi)
      return image
   }
   
   
   
   func addButtonPressed() {
      print("addButtonPressed")
//      xmppclient.sender.pubsubReadAll()

   }
   
   
/*
   func setupCurtain() {
      animator = UIDynamicAnimator(referenceView: view)
      
      curtain = UIView(frame: CGRect(x: 0, y: 20 - screenHeight, width: screenWidth, height: screenHeight))
      curtain.backgroundColor = UIColor.whiteColor()
      
      let curtainLock = UIView(frame: CGRect(x:0, y:curtain.frame.height-44, width:curtain.frame.width, height:44))
      curtainLock.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: "handlePan:"))
      curtainLock.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "curtainUp"))
      let gesture = UISwipeGestureRecognizer(target: self, action: Selector("curtainUp"))
      gesture.direction = .Up
      curtainLock.addGestureRecognizer(gesture)
      curtainLock.backgroundColor = UIColor(hex: 0xf6f6f7)
      curtain.addSubview(curtainLock)
      
      parentVC.view.addSubview(curtain)
      
      filterVC = FilterVC()
      parentVC.addChildViewController(filterVC)
      curtain.addSubview(filterVC.view)
      filterVC.didMoveToparentVCViewController(self)
      
      gravity = UIGravityBehavior(items: [curtain])
      collision = UICollisionBehavior(items: [curtain])
   }
   
   
   
   
   func curtainUp() {
      animator.removeAllBehaviors()
      
      let rotationBan = UIDynamicItemBehavior(items: [curtain])
      rotationBan.allowsRotation = false
      animator.addBehavior(rotationBan)
      
      let snapBehaviour = UISnapBehavior(item: curtain, snapToPoint: CGPointMake(parentVC.screenWidth/2, -curtain.frame.height/2 + 20))
      snapBehaviour.damping = 0.3
      animator.addBehavior(snapBehaviour)
   }
   func handlePan(panGesture: UIPanGestureRecognizer) {
      let obj = panGesture.view?.superview
      let yTranslation = obj!.center.y + panGesture.translationInView(view).y
      panGesture.setTranslation(CGPointZero, inView: view)
      obj!.center = CGPoint(x: obj!.center.x, y: yTranslation)
      obj!.multipleTouchEnabled = true
      obj!.userInteractionEnabled = true
      
      switch panGesture.state {
      case .Began: break
      case .Changed: break
      case .Ended:
         curtainUp()
      default: break
      }
   }
*/
   func curtainDown() {
      print("curtainDown")
//      animator.removeAllBehaviors()
//      
//      collision.translatesReferenceBoundsIntoBoundary = false
//      collision.addBoundaryWithIdentifier("barrier", fromPoint:CGPointMake(0, screenHeight), toPoint: CGPointMake(screenWidth, screenHeight))
//      animator.addBehavior(collision)
//      
//      gravity.magnitude = 12;
//      animator.addBehavior(gravity)
   }


   
}


extension NewsVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return feedArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: FeedCell.self), for: indexPath) as! FeedCell
        cell.view = feedArray[indexPath.row]
        cell.addSubview(cell.view)
        return cell
    }
   
   func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
      let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: FeedCell.self), for: indexPath) as! FeedCell
      cell.view.removeFromSuperview()
   }
   
   
   func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
      return feedArray[indexPath.row].frame.size.height + 10
   }
    
   
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y;
        let headerContentView = self.tableView.tableHeaderView!.subviews[0];
        if offsetY < -10 {
            headerContentView.transform = CGAffineTransform(translationX: 0, y: offsetY); //MIN(offsetY, 0)
        } else {
            headerContentView.transform = CGAffineTransform(translationX: 0, y: -10);
        }
    }
}




class FeedCell: UITableViewCell {
    
   var view = UIView()
    
   override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
      super.init(style: style, reuseIdentifier: reuseIdentifier)
      self.backgroundColor = UIColor.clear
      self.selectionStyle = .none
   }
   
   required init(coder aDecoder: NSCoder) {
      super.init(coder: aDecoder)!
      print("init(coder:) has not been implemented")
   }
}

