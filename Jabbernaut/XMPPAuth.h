//
//  XMPPAuth.h
//  Dashboard
//
//  Created by Mikhail Baynov on 15/04/16.
//  Copyright © 2016 Mikhail Baynov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XMPPDigestMD5Authentication : NSObject

@property (nonatomic, strong) NSString *username;
@property (nonatomic, strong) NSString *password;

- (NSString *)base64EncodedFullResponseWithRealm:(NSString *)realm nonce:(NSString *)nonce cnonce:(NSString*)cnonce qop:(NSString *)qop digestURI:(NSString*)digestURI;

- (NSString *)handleAuth1WithCombinedNonce:(NSString*)combinedNonce salt:(NSString*)salt countStr:(NSString*)countStr;


@end
