import UIKit


class ServerCell: UITableViewCell {
   var view : ServerView!
   
   override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
      super.init(style: style, reuseIdentifier: reuseIdentifier)
      self.backgroundColor = UIColor.clear
      self.selectionStyle = .none
   }
   
   required init?(coder aDecoder: NSCoder) {
      super.init(coder: aDecoder)
   }
}


class ServerView: UIView {
   var checkmarkView : UIImageView!
   var titleLabel : UILabel!
   var stringLabel : UILabel!
   
   var server : String!
   
   
   init(server : String) {
      super.init(frame: CGRect(x:0, y:0, width:UIScreen.main.bounds.width, height:80))
      
      self.server = server
      self.backgroundColor = UIColor(hex: 0xffffff)
      
      checkmarkView = UIImageView(frame:CGRect(x:293, y:27, width:14, height:14))
      checkmarkView.image = UIImage(named:"checkmark")
      checkmarkView.backgroundColor = UIColor.clear
      checkmarkView.isHidden = true
      self.addSubview(checkmarkView)

      
      titleLabel = UILabel(frame:CGRect(x:65, y:7, width:UIScreen.main.bounds.width - 85, height:30))
      titleLabel.text = self.server
      titleLabel.textColor = UIColor(hex: 0x000000)
      titleLabel.font = UIFont(name: "HelveticaNeue-Bold", size: 13)
      self.addSubview(titleLabel)
      
      stringLabel = UILabel(frame:CGRect(x:65, y:34, width:UIScreen.main.bounds.width - 85, height:30))
      stringLabel.numberOfLines = 0
      stringLabel.text = appdata.servers[self.server] as? String
      stringLabel.textColor = UIColor(hex: 0xa9a9a9)
      stringLabel.font = UIFont(name: "HelveticaNeue", size: 13)
      stringLabel.sizeToFit()
      stringLabel.numberOfLines = stringLabel.frame.height > 60 ? 2 : stringLabel.numberOfLines
      stringLabel.sizeToFit()
      self.addSubview(stringLabel)
      
      
      
      self.frame = CGRect(x:0, y:0, width:UIScreen.main.bounds.width, height:stringLabel.frame.height + 52)
      
      
      let separator = UIView(frame: CGRect(x:0, y:self.frame.height-1, width:UIScreen.main.bounds.width, height:1))
      separator.backgroundColor = UIColor(hex: 0xd1d0d4)
      self.addSubview(separator)
   }
   
   required init?(coder aDecoder: NSCoder) {
      super.init(coder: aDecoder)
   }
}

