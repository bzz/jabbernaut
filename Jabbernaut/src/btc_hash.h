#ifndef btc_hash_h
#define btc_hash_h

#include <stdio.h>


#define SHA256_DIGEST_LENGTH        32



void sha256(const uint8_t *message, size_t len, uint8_t digest[32]);
void ripemd160(const uint8_t *message, uint32_t len, uint8_t digest[20]);

///double sha256
void hash256(const uint8_t *message, size_t len, uint8_t digest[32]);

//ripemd160 of sha256
void hash160(uint8_t *message, uint32_t len, uint8_t digest[20]);


void base58_encode(const uint8_t *data, size_t data_len, char *result);


#endif /* btc_hash_h */
