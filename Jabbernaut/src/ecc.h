//
//  ecc.h
//  wallet
//
//  Created by Mikhail Baynov on 19/11/15.
//  Copyright © 2015 Mikhail Baynov. All rights reserved.
//

#ifndef ecc_h
#define ecc_h

#include <stdio.h>

typedef uint32_t uECC_word_t;
typedef uint64_t uECC_dword_t;
typedef unsigned wordcount_t;
typedef int swordcount_t;
typedef int bitcount_t;
typedef int cmpresult_t;

#define HIGH_BIT_SET 0x80000000
#define uECC_WORD_BITS 32
#define uECC_WORD_BITS_SHIFT 5
#define uECC_WORD_BITS_MASK 0x01F


#define uECC_WORDS 8
#define uECC_BYTES 32

typedef struct EccPoint {
    uECC_word_t x[uECC_WORDS];
    uECC_word_t y[uECC_WORDS];
} EccPoint;


// Get the public key from the private key
void uECC_get_public_key65(const uint8_t p_privateKey[uECC_BYTES],
                           uint8_t p_publicKey[uECC_BYTES * 2 + 1]);
void uECC_get_public_key64(const uint8_t p_privateKey[uECC_BYTES],
                           uint8_t p_publicKey[uECC_BYTES * 2]);
void uECC_get_public_key33(const uint8_t p_privateKey[uECC_BYTES],
                           uint8_t p_publicKey[uECC_BYTES + 1]);

// Check if the private key is not equal to 0 and less than the order
// Returns 1 if valid
int uECC_isValid(uint8_t *p_key);


// Generate an ECDSA signature for a given hash value.
// Returns 0 always.
int uECC_sign_digest(const uint8_t p_privateKey[uECC_BYTES],
                     const uint8_t p_hash[uECC_BYTES],
                     uint8_t p_signature[uECC_BYTES * 2]);

// Verify an ECDSA signature.
// Returns 0 if the signature is valid, 1 if it is invalid.
int uECC_verify_digest(const uint8_t p_publicKey[uECC_BYTES * 2],
                       const uint8_t p_hash[uECC_BYTES],
                       const uint8_t p_signature[uECC_BYTES * 2]);



int DER_encode_digest(const uint8_t signature[uECC_BYTES * 2],
                            size_t  *der_signature_len,
                            uint8_t **der_signature);

#endif /* ecc_h */
