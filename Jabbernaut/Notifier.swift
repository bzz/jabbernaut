import Foundation

public enum NotifierEvent : String {
   case authMechanismsFound = "authMechanismsFound"
   case authSucceed = "authSucceed"
   case authFailed = "authFailed"
   case bindFeatureFound = "bindFeatureFound"
   case boundResource = "boundResource"
   case friendOnline = "friendOnline"
   case friendOffline = "friendOffline"
   case friendAway = "friendAway"
   case gotRoster = "gotRoster"
   case gotVCARD = "gotVCARD"
   case gotSession = "gotSession"
   case gotMessage = "gotMessage"
   case gotMessageShow = "gotMessageShow"
   case gotPresence = "gotPresence"
   case gotPong = "gotPong"
   case gotPostsubNodes = "gotPostsubNodes"   
   case jabberReady = "jabberReady"
   
   case messageDeliveryError = "messageDeliveryError"
   case messageReceiptConfirmed = "messageReceiptConfirmed"
   case messageReceiptConfirmedReloadData = "messageReceiptConfirmedReloadData"
   case newContactAdded = "newContactAdded"
   case proceedWithTLS = "proceedWithTLS"
   case registrationSucceed = "registrationSucceed"
   case registrationFailedTooFast = "registrationFailedTooFast"
   case registrationFailedTaken = "registrationFailedTaken"
   case registrationFailedNotAllowed = "registrationFailedNotAllowed"
   case serverStatusStringChanged = "serverStatusStringChanged"
   case tlsFeatureFound = "tlsFeatureFound"
   case tlsRequired = "tlsRequired"
   
   case networkOff = "networkOff"
   
}



public final class Notifier { 

   public static func post(_ event: NotifierEvent) {
      print("NOTIFIER: \(event.rawValue)")
      NotificationCenter.default.post(name: Notification.Name(rawValue: event.rawValue), object:self)
   }   
   public static func post(_ event: NotifierEvent, userInfo: [AnyHashable: Any]) {
      print("NOTIFIER: \(event.rawValue) \(userInfo.description)")
      NotificationCenter.default.post(name: Notification.Name(rawValue: event.rawValue), object:self, userInfo:userInfo)
   }
   
   
   public static func addObserver(_ observer: AnyObject, event : NotifierEvent) {
      NotificationCenter.default.addObserver(observer, selector: Selector(event.rawValue + ":"), name: NSNotification.Name(rawValue: event.rawValue), object :nil)
   }
   
   public static func removeObserver(_ observer: AnyObject, event : NotifierEvent) {
      NotificationCenter.default.removeObserver(observer, name: NSNotification.Name(rawValue: event.rawValue), object: nil)
   }

}
