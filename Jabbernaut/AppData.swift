import UIKit



open class AppData : NSObject {
   
   var history = NSMutableDictionary()  //  address or jid :
   //  [ UTCString::from_jid or 'MYSELF')::body::messageID_or_'SENT'] ]
   var buddies = NSMutableDictionary()  //  [ address_or_jid : [address::last_jid::name::position::APNStoken]   ]
   var avatars = NSMutableDictionary()  //  [ address_or_jid : AvatarString ]

   var servers = NSMutableDictionary()  //  [ server : String]
   
   

   //returns jid on fail
   open func buddiesKeyForJID(_ jid : String) -> String {
      let arr = jid.components(separatedBy: "@")
      guard arr.count > 1 else { return "" }
      let server = arr[1]
      for key in buddies.allKeys {
         if let k = key as? String {
            if !k.contains(".") {
               let login = btcmanager.getXMPPLogin(fromAddress: k, server: server)
               if login! + "@" + server == jid {
                  return k
               }
            }
         }
      }
      if xmppclient.model.jid == jid {
         return addressString
      }
      return jid
   }
   
   open func randomizeSecretHEXString() {
      let bytesCount = 32
      var randomBytes = [UInt8](repeating: 0, count: bytesCount)
      SecRandomCopyBytes(kSecRandomDefault, bytesCount, &randomBytes)
      appdata.secretHEXString = randomBytes.map({String(format: "%02X", $0)}).joined(separator: "")
   }
   
   
   
   var secretHEXString: String {
      get {
         guard let out = UserDefaults.standard.object(forKey: "hexString") as? String else {
            return ""
         }
         return out
      }
      set {
         UserDefaults.standard.set(newValue, forKey:"hexString")
         UserDefaults.standard.synchronize()
         if let data = secretHEXString.dataFromHexadecimalString() {
            addressString = btcmanager.address(fromSecretKey: data)
         }
      }
   }
   var addressString = ""  //derived from hexString
   
   
   var APNSToken: String {
      get {
         guard let out = UserDefaults.standard.object(forKey: "APNSToken") as? String else {
            return ""
         }
         return out
      }
      set {
         UserDefaults.standard.set(newValue, forKey:"APNSToken")
         UserDefaults.standard.synchronize()
      }
   }
   
   var userInfoString: String {
      get {
         guard let out = UserDefaults.standard.object(forKey: "userInfoString") as? String else {
            let buddy = Buddy()
            if xmppclient.model != nil {
               let arr = xmppclient.model.jid.components(separatedBy: "@")
               buddy.name = arr[0]
               buddy.address = addressString
               buddy.last_jid = xmppclient.model.jid
            }
            return buddy.string
         }
         return out
      }
      set {
         UserDefaults.standard.set(newValue, forKey:"userInfoString")
         UserDefaults.standard.synchronize()
      }
   }
   
   
   var userAvatar : UIImage {
      get {
         if let out = UIImage(data: Data.init(base64Encoded: appdata.userAvatarString, options: NSData.Base64DecodingOptions.ignoreUnknownCharacters)!) {
            return out
         }
         return UIImage()
      }
      set {
         if let data = UIImageJPEGRepresentation(newValue, 1.0) {
            let str = data.base64EncodedString(options: [])
            UserDefaults.standard.set(str, forKey:"userAvatarString")
            UserDefaults.standard.synchronize()
         }
      }
   }
   var userAvatarString: String {
      get {
         guard let out = UserDefaults.standard.object(forKey: "userAvatarString") as? String else {
            return ""
         }
         return out
      }
   }
   

   
   
   
   func saveDic(_ dic: AnyObject, userDefaultsKey : String) {
      print("SAVING " + userDefaultsKey)
      do {
         //         print(history.description)
         let data = try PropertyListSerialization.data(fromPropertyList: dic, format:PropertyListSerialization.PropertyListFormat.xml, options: 0)
         UserDefaults.standard.set(data, forKey:userDefaultsKey)
         UserDefaults.standard.synchronize()
      } catch {
         print("Error reading plist: \(error)")
      }
   }
   
   func loadDic(_ userDefaultsKey : String) -> NSMutableDictionary {
      print("LOAD " + userDefaultsKey)
      var out : NSMutableDictionary?
      var format = PropertyListSerialization.PropertyListFormat.xml
      if let plistXML = UserDefaults.standard.object(forKey: userDefaultsKey) as? Data {
         do {
            out = try PropertyListSerialization.propertyList(from: plistXML, options: .mutableContainersAndLeaves, format: &format) as? NSMutableDictionary
         }
         catch {
            print("Error reading plist: \(error), format: \(format)")
         }
      }
      if out != nil {
         return out!
      } else {
         return NSMutableDictionary()
      }
   }
   
   func saveAll() {
      saveDic(history, userDefaultsKey: "history")
      saveDic(buddies, userDefaultsKey: "buddies")
      saveDic(avatars, userDefaultsKey: "avatars")
      saveDic(servers, userDefaultsKey: "servers")
   }

   func loadAll() {
      history = loadDic("history")
      buddies = loadDic("buddies")
      avatars = loadDic("avatars")
      servers = loadDic("servers")
   }
   
   open func clearCache() {
      history = NSMutableDictionary()
      buddies = NSMutableDictionary()
      avatars = NSMutableDictionary()
      servers = NSMutableDictionary()
      saveDic(history, userDefaultsKey: "history")
      saveDic(buddies, userDefaultsKey: "buddies")
      saveDic(avatars, userDefaultsKey: "avatars")
      saveDic(servers, userDefaultsKey: "servers")
   }

}
