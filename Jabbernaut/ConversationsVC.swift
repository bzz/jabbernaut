import UIKit


class ConversationsVC: UIViewController {
   
   var tableView = UITableView()
   var tableViewArray: [ConversationView] = []
   
   var parentVC : NavigationVC!

   
   
   override func viewDidAppear(_ animated: Bool) {
      UIApplication.shared.applicationIconBadgeNumber = 0
   }
   

   
   override func viewDidLoad() {

      parentVC = parent as! NavigationVC
      view.frame = parentVC.mainViewBed.bounds
      
      view.backgroundColor = UIColor(hexA:0xffffff10)
      
      parentVC.newMessageIndicator.backgroundColor = UIColor.clear

      tableView = UITableView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height))
      tableView.delegate = self
      tableView.dataSource = self
      tableView.register(ConversationCell.self, forCellReuseIdentifier: String(describing: ConversationCell.self))
      tableView.backgroundColor = UIColor.clear
      tableView.showsVerticalScrollIndicator = false
      tableView.separatorStyle = .none
      tableView.rowHeight = UITableViewAutomaticDimension;
      view.addSubview(tableView)
      //        tableView.estimatedRowHeight = 100.0;
      
      
      parentVC.removeAddButton()
      parentVC.setupTitleButton("Conversations", titleColor: UIColor(hex: 0x000000), font: UIFont(name: "HelveticaNeue-Bold", size: 16)!, backgroundColor: UIColor(hex: 0xf7f7f7), target:self)
      
      
      reloadTableView()
      initNotifications()

      print("appDelegate.history")
      print(appdata.history)
      
   
   }
   
   
   func initNotifications() {
      Notifier.addObserver(self, event: .gotMessageShow)
      
      Notifier.addObserver(self, event: .friendOnline)
      Notifier.addObserver(self, event: .friendOffline)
      Notifier.addObserver(self, event: .friendAway)
      Notifier.addObserver(self, event: .gotVCARD)
   }
   deinit {
      Notifier.removeObserver(self, event: .gotMessageShow)

      Notifier.removeObserver(self, event: .friendOnline)
      Notifier.removeObserver(self, event: .friendOffline)
      Notifier.removeObserver(self, event: .friendAway)
      Notifier.removeObserver(self, event: .gotVCARD)
   }

   
   func gotMessageShow(_ notif : Notification) {
      reloadTableView()
   }

   func friendOnline(_ notif : Notification) {
      reloadTableView()
   }
   func friendAway(_ notif : Notification) {
      reloadTableView()
   }
   func friendOffline(_ notif : Notification) {
      reloadTableView()
   }
   func gotVCARD(_ notif : Notification) {
      reloadTableView()
   }
   
   
   func reloadTableView() {
      tableViewArray.removeAll()
      for key in appdata.history.allKeys {
         tableViewArray.append(ConversationView(key:key as! String))
      }      
      tableView.reloadData()
   }
   

   func dpiScale(_ image:UIView!) -> UIView! {
      let dpi = screenWidth / 320
      image.frame = CGRect(x:image.frame.origin.x, y:image.frame.origin.y, width:image.frame.size.width * dpi, height:image.frame.size.height * dpi)
      return image
   }

   
   
   
   

}




extension ConversationsVC: UITableViewDelegate, UITableViewDataSource {
   
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return tableViewArray.count
   }
   
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ConversationCell.self)) as! ConversationCell
      cell.view = tableViewArray[indexPath.row]
      cell.addSubview(cell.view)
      return cell
   }
   
   func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
      let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ConversationCell.self)) as! ConversationCell
      cell.view.removeFromSuperview()
   }
   
   
   func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
      return tableViewArray[indexPath.row].frame.size.height
   }
   
   
   func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
      return true
   }
   
   
   func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      if xmppclient.authmodel.jabber_ready {
         appDelegate.window?.rootViewController = ChatVC(jid:tableViewArray[indexPath.row].jid)
      }
   }

   
   func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
      if (editingStyle == UITableViewCellEditingStyle.delete) {
         tableView.beginUpdates()
         appdata.history.removeObject(forKey: tableViewArray[indexPath.row].jid)
         tableViewArray.remove(at: indexPath.row)
         tableView.deleteRows(at: [indexPath], with: .fade)
         tableView.endUpdates()
      }
   }
}
