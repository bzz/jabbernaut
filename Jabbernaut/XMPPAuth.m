//
//  XMPPAuth.m
//  Dashboard
//
//  Created by Mikhail Baynov on 15/04/16.
//  Copyright © 2016 Mikhail Baynov. All rights reserved.
//

#import	<CommonCrypto/CommonHMAC.h>
#import	<CommonCrypto/CommonDigest.h>
#import	<CommonCrypto/CommonCryptor.h>

#import "XMPPAuth.h"
#import "NSData+XMPP.h"


@interface XMPPDigestMD5Authentication (){}
@property (nonatomic, strong) NSString *combinedNonce;
@property (nonatomic, strong) NSString *salt;
@property (nonatomic, strong) NSNumber *count;


//@property (nonatomic) BOOL awaitingChallenge;
//@property (nonatomic, strong) NSString *clientNonce;

@property (nonatomic, strong) NSString *serverMessage1;
@property (nonatomic, strong) NSString *clientFirstMessageBare;

@property (nonatomic, strong) NSData *serverSignatureData;
@property (nonatomic, strong) NSData *clientProofData;
@property (nonatomic) CCHmacAlgorithm hashAlgorithm;




@end




@implementation XMPPDigestMD5Authentication





- (NSString *)base64EncodedFullResponseWithRealm:(NSString *)realm nonce:(NSString *)nonce cnonce:(NSString*)cnonce qop:(NSString *)qop digestURI:(NSString*)digestURI {
   NSString *HA1str = [NSString stringWithFormat:@"%@:%@:%@", _username, realm, _password];
   NSString *HA2str = [NSString stringWithFormat:@"AUTHENTICATE:%@", digestURI];
   NSData *HA1dataA = [[HA1str dataUsingEncoding:NSUTF8StringEncoding] xmpp_md5Digest];
   NSData *HA1dataB = [[NSString stringWithFormat:@":%@:%@", nonce, cnonce] dataUsingEncoding:NSUTF8StringEncoding];

   NSMutableData *HA1data = [NSMutableData dataWithCapacity:([HA1dataA length] + [HA1dataB length])];
   [HA1data appendData:HA1dataA];
   [HA1data appendData:HA1dataB];
   
   NSString *HA1 = [[HA1data xmpp_md5Digest] xmpp_hexStringValue];
   NSString *HA2 = [[[HA2str dataUsingEncoding:NSUTF8StringEncoding] xmpp_md5Digest] xmpp_hexStringValue];
   
   NSString *responseStr = [NSString stringWithFormat:@"%@:%@:00000001:%@:auth:%@", HA1, nonce, cnonce, HA2];
   NSString *response = [[[responseStr dataUsingEncoding:NSUTF8StringEncoding] xmpp_md5Digest] xmpp_hexStringValue];

   
   NSMutableString *buffer = [NSMutableString stringWithCapacity:1024];
   [buffer appendFormat:@"username=\"%@\",", _username];
   [buffer appendFormat:@"realm=\"%@\",", realm];
   [buffer appendFormat:@"nonce=\"%@\",", nonce];
   [buffer appendFormat:@"cnonce=\"%@\",", cnonce];
   [buffer appendFormat:@"nc=00000001,"];
   [buffer appendFormat:@"qop=auth,"];
   [buffer appendFormat:@"digest-uri=\"%@\",", digestURI];
   [buffer appendFormat:@"response=%@,", response];
   [buffer appendFormat:@"charset=utf-8"];
   
   
   NSData *utf8data = [buffer dataUsingEncoding:NSUTF8StringEncoding];
   
   return [utf8data xmpp_base64Encoded];
}



- (NSString *)handleAuth1WithCombinedNonce:(NSString*)combinedNonce salt:(NSString*)salt countStr:(NSString*)countStr
{
   
   // We're expecting a challenge response.
   // If we get anything else we're going to assume it's some kind of failure response.
   
//   if (![[authResponse name] isEqualToString:@"challenge"])
//   {
//      return XMPP_AUTH_FAIL;
//   }
//   
//   NSDictionary *auth = [self dictionaryFromChallenge:authResponse];
//   
   NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
   [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
   
   self.combinedNonce = combinedNonce;
   self.salt = salt;
   self.count = [numberFormatter numberFromString:countStr];
   
   
   //We have all the necessary information to calculate client proof and server signature


   if (!self.password.length || !self.salt.length || self.count.unsignedIntegerValue < 4096) {
      return @"";
   }
   
   NSData *passwordData = [self.password dataUsingEncoding:NSUTF8StringEncoding];
   NSData *saltData = [[self.salt dataUsingEncoding:NSUTF8StringEncoding] xmpp_base64Decoded];
   NSData *saltedPasswordData = [self HashWithAlgorithm:self.hashAlgorithm password:passwordData salt:saltData iterations:[self.count unsignedIntValue]];

   NSLog(@"passwordData %@", passwordData.xmpp_hexStringValue);
   NSLog(@"saltData %@", saltData.xmpp_hexStringValue);
   NSLog(@"saltedPasswordData %@", saltedPasswordData.xmpp_hexStringValue);

   NSData *clientKeyData = [self HashWithAlgorithm:self.hashAlgorithm data:[@"Client Key" dataUsingEncoding:NSUTF8StringEncoding] key:saltedPasswordData];
   NSData *serverKeyData = [self HashWithAlgorithm:self.hashAlgorithm data:[@"Server Key" dataUsingEncoding:NSUTF8StringEncoding] key:saltedPasswordData];
   NSLog(@"clientKeyData %@", clientKeyData.xmpp_hexStringValue);
   NSLog(@"serverKeyData %@", serverKeyData.xmpp_hexStringValue);


   NSData *storedKeyData = [clientKeyData xmpp_sha1Digest];
   NSLog(@"storedKeyData %@", storedKeyData.xmpp_hexStringValue);
   
   NSData *authMessageData = [[NSString stringWithFormat:@"%@,%@,c=biws,r=%@",self.clientFirstMessageBare,self.serverMessage1,self.combinedNonce] dataUsingEncoding:NSUTF8StringEncoding];
   NSLog(@"authMessageData %@", authMessageData.xmpp_hexStringValue);
   
   NSData *clientSignatureData = [self HashWithAlgorithm:self.hashAlgorithm data:authMessageData key:storedKeyData];
   NSLog(@"clientSignatureData %@", clientSignatureData.xmpp_hexStringValue);
   
   self.serverSignatureData = [self HashWithAlgorithm:self.hashAlgorithm data:authMessageData key:serverKeyData];
   self.clientProofData = [self xorData:clientKeyData withData:clientSignatureData];
   NSLog(@"_serverSignatureData %@", _serverSignatureData.xmpp_hexStringValue);
   NSLog(@"_clientProofData %@", _clientProofData.xmpp_hexStringValue);
   
   //check to see that we caclulated some client proof and server signature
   if (self.clientProofData && self.serverSignatureData) {
      
      NSLog(@"self.clientMessage2 string  %@", self.clientMessage2);
      return [self clientMessage2];
      //      NSXMLElement *response = [NSXMLElement elementWithName:@"response" xmlns:@"urn:ietf:params:xml:ns:xmpp-sasl"];
      //      [response setStringValue:[self clientMessage2]];
      //
      //      [xmppStream sendAuthElement:response];
      //      self.awaitingChallenge = NO;
   }
   return @"";
}

/*
- (NSDictionary *)dictionaryFromChallenge:(NSXMLElement *)challenge
{
   // The value of the challenge stanza is base 64 encoded.
   // Once "decoded", it's just a string of key=value pairs separated by commas.
   
   NSData *base64Data = [[challenge stringValue] dataUsingEncoding:NSASCIIStringEncoding];
   NSData *decodedData = [base64Data xmpp_base64Decoded];
   
   self.serverMessage1 = [[NSString alloc] initWithData:decodedData encoding:NSUTF8StringEncoding];
   
   
   NSArray *components = [self.serverMessage1 componentsSeparatedByString:@","];
   NSMutableDictionary *auth = [NSMutableDictionary dictionaryWithCapacity:5];
   
   for (NSString *component in components)
   {
      NSRange separator = [component rangeOfString:@"="];
      if (separator.location != NSNotFound)
      {
         NSMutableString *key = [[component substringToIndex:separator.location] mutableCopy];
         NSMutableString *value = [[component substringFromIndex:separator.location+1] mutableCopy];
         
         if(key) CFStringTrimWhitespace((__bridge CFMutableStringRef)key);
         if(value) CFStringTrimWhitespace((__bridge CFMutableStringRef)value);
         
         if ([value hasPrefix:@"\""] && [value hasSuffix:@"\""] && [value length] > 2)
         {
            // Strip quotes from value
            [value deleteCharactersInRange:NSMakeRange(0, 1)];
            [value deleteCharactersInRange:NSMakeRange([value length]-1, 1)];
         }
         
         if(key && value)
         {
            auth[key] = value;
         }
      }
   }
   
   return auth;
}
*/





- (NSString *)clientMessage2
{
   NSString *clientProofString = [self.clientProofData xmpp_base64Encoded];
   NSData *message2Data = [[NSString stringWithFormat:@"c=biws,r=%@,p=%@",self.combinedNonce,clientProofString] dataUsingEncoding:NSUTF8StringEncoding];
   
   return [message2Data xmpp_base64Encoded];
}



- (NSData *)HashWithAlgorithm:(CCHmacAlgorithm) algorithm password:(NSData *)passwordData salt:(NSData *)saltData iterations:(NSUInteger)rounds {
   NSMutableData *mutableSaltData = [saltData mutableCopy];
   UInt8 zeroHex= 0x00;
   UInt8 oneHex= 0x01;
   NSData *zeroData = [[NSData alloc] initWithBytes:&zeroHex length:sizeof(zeroHex)];
   NSData *oneData = [[NSData alloc] initWithBytes:&oneHex length:sizeof(oneHex)];
   
   [mutableSaltData appendData:zeroData];
   [mutableSaltData appendData:zeroData];
   [mutableSaltData appendData:zeroData];
   [mutableSaltData appendData:oneData];
   
   NSData *result = [self HashWithAlgorithm:algorithm data:mutableSaltData key:passwordData];
   NSData *previous = [result copy];
   
   for (int i = 1; i < rounds; i++) {
      previous = [self HashWithAlgorithm:algorithm data:previous key:passwordData];
      result = [self xorData:result withData:previous];
   }
   return result;
}
- (NSData *)HashWithAlgorithm:(CCHmacAlgorithm) algorithm data:(NSData *)data key:(NSData *)key {
   unsigned char cHMAC[CC_SHA1_DIGEST_LENGTH];
   CCHmac(algorithm, [key bytes], [key length], [data bytes], [data length], cHMAC);
   return [[NSData alloc] initWithBytes:cHMAC length:sizeof(cHMAC)];
}
- (NSData *)xorData:(NSData *)data1 withData:(NSData *)data2 {
   NSMutableData *result = data1.mutableCopy;
   char *dataPtr = (char *)result.mutableBytes;
   char *keyData = (char *)data2.bytes;
   char *keyPtr = keyData;
   int keyIndex = 0;
   for (int x = 0; x < data1.length; x++) {
      *dataPtr = *dataPtr ^ *keyPtr;
      dataPtr++;
      keyPtr++;
      
      if (++keyIndex == data2.length) {
         keyIndex = 0;
         keyPtr = keyData;
      }
   }
   return result;
}





@end
