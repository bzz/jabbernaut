import UIKit


class ChatVC : UIViewController {
   var topPanel : UIView!
   var titleLabel : UILabel!
   var statusLabel : UILabel!
   
   var bottomPanel:UIView!
   let textField = UITextField()

   var tableView = UITableView()
   var tableViewArray = [MessageView]()
   var chatHistory = NSMutableArray()
   
   var buddy : Buddy!
   

   
   var tableViewRect = CGRect.zero
   var topPanelRect = CGRect.zero
   var keyboard_moved = false
   var avatar : UIImage?
   
   
   init(jid : String) {
      super.init(nibName: nil, bundle: nil)
      buddy = Buddy(key: jid)

      buddy.last_jid = jid
      
      if let avatar_string = appdata.avatars[appdata.buddiesKeyForJID(jid)] as? String {
         let img_data = Data.init(base64Encoded: avatar_string, options: .ignoreUnknownCharacters)
         avatar = UIImage(data: img_data!)
      }


      
      print("ChatVC init(jid : String) ")
      print(jid)
      
      xmppclient.sender.subscribePresence(buddy.last_jid)
      xmppclient.sender.requestSubscription(buddy.last_jid)
      
      if !xmppclient.model.friendGotVCARD(buddy.last_jid) {
         xmppclient.sender.getVCARD(buddy.last_jid)
      }
   }
   
   required init?(coder aDecoder: NSCoder) {
      super.init(coder: aDecoder)
   }

   
   
   
   override func viewDidLoad() {
      super.viewDidLoad()
      
      
      let background = UIImageView(frame:view.bounds)
      background.image = UIImage(named:"background");
      view.addSubview(background)
      
      
      
      tableViewRect = CGRect(x: 0, y: 64, width: view.frame.width, height: view.frame.height - 64 - 44)
      topPanelRect = CGRect(x: 0, y: 0, width: screenWidth, height: 64)
      

      tableView = UITableView(frame: tableViewRect)
      tableView.delegate = self
      tableView.dataSource = self
      tableView.register(ChatMessageCell.self, forCellReuseIdentifier: String(describing: ChatMessageCell.self))
      tableView.backgroundColor = UIColor.clear
      tableView.showsVerticalScrollIndicator = false
      tableView.separatorStyle = .none
      tableView.rowHeight = UITableViewAutomaticDimension;
      tableView.allowsMultipleSelection = true
      tableView.contentInset = UIEdgeInsetsMake(topPanelRect.size.height, 0, 0, 0)
      tableView.contentOffset.y = CGFloat.greatestFiniteMagnitude
      view.addSubview(tableView)
      
      
      
      
      let tap = UITapGestureRecognizer(target: self, action:#selector(ChatVC.tapRemoveKeyboard))
      tableView.addGestureRecognizer(tap)

      
      
      topPanel = UIView(frame: topPanelRect)
      topPanel.backgroundColor = UIColor(hex: 0xf6f6f7)
      view.addSubview(topPanel);

      
      let backButton = UIButton(frame: CGRect(x: 0, y: 20, width: 44, height: 44))
      backButton.setBackgroundImage(UIImage(named: "icon_back"), for: UIControlState())
      backButton.addTarget(self, action:#selector(backButtonPressed), for: UIControlEvents.touchUpInside)
      topPanel.addSubview(backButton)
      
      
      titleLabel = UILabel(frame: CGRect(x: 44, y: 20, width: screenWidth-88, height: 24))
      titleLabel.backgroundColor = UIColor(hex: 0xf7f7f7)
      titleLabel.textAlignment = .center
      titleLabel.textColor = UIColor(hex: 0x000000)
      titleLabel.font = UIFont(name: "HelveticaNeue-Bold", size: 16)
      topPanel.addSubview(titleLabel)

      statusLabel = UILabel(frame: CGRect(x: 44, y: 44, width: screenWidth-88, height: 20))
      statusLabel.backgroundColor = UIColor(hex: 0xf7f7f7)
      statusLabel.textAlignment = .center
      statusLabel.textColor = UIColor(hex: 0x000000)
      statusLabel.font = UIFont(name: "Courier-Bold", size: 8)
      topPanel.addSubview(statusLabel)
      
      if xmppclient.model.isOnline(buddy.last_jid) {
         statusLabel.text = "ONLINE"
      } else {
         statusLabel.text = "OFFLINE"
         statusLabel.font = UIFont(name: "Courier-Bold", size: 8)
      }

      
      
      
      
      let settingsButton = UIButton(frame: CGRect(x: screenWidth-44, y: 20, width: 44, height: 44))
      settingsButton.setBackgroundImage(UIImage(named: "icon_settings"), for: UIControlState())
      settingsButton.addTarget(self, action:#selector(settingsButtonPressed), for: UIControlEvents.touchUpInside)
      topPanel.addSubview(settingsButton)
      
      
      bottomPanel = UIView(frame: CGRect(x: 0, y: screenHeight-44, width: screenWidth, height: 132))
      bottomPanel.backgroundColor = UIColor(hex: 0xffffff)
      view.addSubview(bottomPanel);

      
      let attachButton = UIButton(frame: CGRect(x: 0, y: 0, width: 55, height: 44))
      attachButton.setBackgroundImage(UIImage(named: "icon_chat_attach"), for: UIControlState())
      attachButton.addTarget(self, action:#selector(attachButtonPressed), for: .touchUpInside)
      bottomPanel.addSubview(attachButton)
      

      let sendButton = UIButton(frame: CGRect(x: screenWidth - 99, y: 0, width: 99, height: 44))
      sendButton.setTitle("Send", for: UIControlState())
      sendButton.setTitleColor(UIColor.black, for: UIControlState())
      sendButton.titleLabel?.font = UIFont(name: "HelveticaNeue-Bold", size: 10)
      sendButton.backgroundColor = UIColor(hex: 0xf7f7f7)
      sendButton.addTarget(self, action:#selector(sendButtonPressed), for: .touchUpInside)
      bottomPanel.addSubview(sendButton)
    
      textField.frame = CGRect(x: 55, y: 8, width: screenWidth - 154, height: 29)
      textField.backgroundColor = UIColor(hex: 0xf9f9f9)
      textField.translatesAutoresizingMaskIntoConstraints = false
      textField.delegate = self
      textField.tag = 1
      textField.textAlignment = NSTextAlignment.center
      textField.textColor = UIColor(hex: 0x32476a)
      textField.font = UIFont(name: "HelveticaNeue-Light", size: 14)
      textField.layer.cornerRadius = 5
      textField.layer.borderWidth = 1
      textField.layer.borderColor = UIColor(hex: 0xc6c7cb).cgColor
      textField.placeholder = "Сообщение"
      bottomPanel.addSubview(textField)
      
      
      
      if buddy.address == "" {
         titleLabel.text = buddy.last_jid
      } else {
         titleLabel.text = buddy.screen_name
      }
      chatHistory = buddy.history()
      reloadTableViewArray()
      
      initNotifications()
   }

   
   func initNotifications() {
      Notifier.addObserver(self, event: .friendOnline)
      Notifier.addObserver(self, event: .friendOffline)
      Notifier.addObserver(self, event: .friendAway)
      Notifier.addObserver(self, event: .gotMessageShow)
      Notifier.addObserver(self, event: .messageReceiptConfirmedReloadData)
      Notifier.addObserver(self, event: .gotVCARD)
      NotificationCenter.default.addObserver(self, selector: #selector(ChatVC.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
      NotificationCenter.default.addObserver(self, selector: #selector(ChatVC.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
   }
   
   deinit {
      Notifier.removeObserver(self, event: .friendOnline)
      Notifier.removeObserver(self, event: .friendOffline)
      Notifier.removeObserver(self, event: .friendAway)
      Notifier.removeObserver(self, event: .gotMessageShow)
      Notifier.removeObserver(self, event: .messageReceiptConfirmedReloadData)
      Notifier.removeObserver(self, event: .gotVCARD)
      NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
      NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
   }

   func reloadTableViewArray() {
      tableViewArray.removeAll()
      for m in chatHistory {
         let mess = Message(string: m as! String)

         var outgoing = false
         var read = false
         
         if mess.from == "MYSELF" {
            outgoing = true
            if mess.messageID == "SENT" {
               read = true
            }
         } else {
            read = true
         }
         
         let icon = outgoing ? appdata.userAvatar : avatar
         
         tableViewArray.append(MessageView(string:mess.body, time:mess.date, icon:icon, outgoing:outgoing, read:read))
      }
      tableView.reloadData()
   }
   
   

   func friendOnline(_ notif : Notification) {
      if let from = notif.userInfo?["from"] as? String {
         let jid = from.components(separatedBy: "/")[0]
         if jid == buddy.last_jid {
            statusLabel.text = "ONLINE"
         }
      }
   }
   func friendAway(_ notif : Notification) {
      if let from = notif.userInfo?["from"] as? String {
         let jid = from.components(separatedBy: "/")[0]
         if jid == buddy.last_jid {
            statusLabel.text = "AWAY"
         }
      }
   }
   func friendOffline(_ notif : Notification) {
      if let from = notif.userInfo?["from"] as? String {
         let jid = from.components(separatedBy: "/")[0]
         if jid == buddy.last_jid {
            statusLabel.text = "OFFLINE"
         }
      }
   }
   
   
   func gotMessageShow(_ notif : Notification) {
      if let jid = notif.userInfo?["from"] as? String {
         buddy.last_jid = jid
         
         if buddy.address == appdata.buddiesKeyForJID(jid) {

            print("buddy.history()")
            print(buddy.history())
            
            let mess = Message(string: buddy.history().lastObject as! String)
            let icon = UIImage(); //TODO: GET ICON FROM jid
            tableViewArray.append(MessageView(string:mess.body, time:mess.date, icon:icon, outgoing:false, read:true))
            
            buddy.saveHistory(chatHistory)
            
            if tableViewArray.count == chatHistory.count {
               let indexPath = IndexPath(row: chatHistory.count - 1, section: 0)
               self.tableView.insertRows(at: [indexPath] , with: UITableViewRowAnimation.left)
               self.tableView.scrollToRow(at: indexPath, at: UITableViewScrollPosition.bottom, animated:true)
            } else {
               reloadTableViewArray()
            }

            
          }
      }
   }
   
   
   func messageReceiptConfirmedReloadData(_ notif : Notification) {
      self.reloadTableViewArray()
   }
   
   func gotVCARD(_ notif : Notification) {
      guard let jid = notif.userInfo?["from"] as? String else { return }
      
      buddy = Buddy(key: jid)
      if buddy.address == "" {
         titleLabel.text = buddy.last_jid
      } else {
         titleLabel.text = buddy.screen_name
      }
      chatHistory = buddy.history()
      if let avatar_string = appdata.avatars[jid] as? String {
         let img_data = Data.init(base64Encoded: avatar_string, options: .ignoreUnknownCharacters)
         avatar = UIImage(data: img_data!)
      }
      reloadTableViewArray()
   }
   


   
   
   
   
   
   
   func backButtonPressed() {
      print("backButtonPressed")
      if tableViewArray.count > 0 {
         appDelegate.window?.rootViewController = NavigationVC(buttonIndex:1)
      } else {
         appDelegate.window?.rootViewController = NavigationVC(buttonIndex: 2)
      }
   }
   
   
   func settingsButtonPressed() {
      print("settingsButtonPressed")
   }
   
   
   
   func attachButtonPressed() {
      print("attachButtonPressed")
   }
   func sendButtonPressed() {
      textFieldShouldReturn(textField)
   }
   
}




extension ChatVC: UITableViewDelegate, UITableViewDataSource {
   
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return chatHistory.count
   }
   
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ChatMessageCell.self), for: indexPath) as! ChatMessageCell

      cell.view = tableViewArray[indexPath.row]// chatHistory![indexPath.row].view
      
      cell.addSubview(cell.view)
      return cell
   }
   
   func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
      let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ChatMessageCell.self), for: indexPath) as! ChatMessageCell
      cell.view.removeFromSuperview()
   }
   
   
   func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
      guard indexPath.row < tableViewArray.count else { return 0 }
      return tableViewArray[indexPath.row].frame.size.height
   }
   
   
   
   
   
   func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
   }
}


extension ChatVC: UITextFieldDelegate {
   
   func textFieldShouldReturn(_ textField: UITextField) -> Bool {
      textField.resignFirstResponder()
      

      
      if (textField.text != "") {
         //if offline
         if !xmppclient.model.isOnline(buddy.last_jid) {
            if let buddyString = appdata.buddies[buddy.address] as? String {
               let buddy = Buddy(string: buddyString)
               self.buddy.APNSToken = buddy.APNSToken
               if buddy.APNSToken.isEmpty {
                  print("Empty token for " + buddy.address)
               } else {
                  
                  let aps : [String : Any] = ["sound":"default", "alert":"\(buddy.name): \(textField.text!)", "badge":1, "type":"message", "from":xmppclient.model.jid]
                  APNSNetwork().pushPayload(["aps":aps], token: buddy.APNSToken)
               }
            }
         }

         
         
         print("textField message:" + textField.text!)
         
         let messageID = String.randomStringWithLength(12)
         xmppclient.sender.sendMessage(textField.text!, toJID:buddy.last_jid, messageID:messageID)
         
         if xmppclient.authmodel.jabber_ready {
            let body = textField.text!
            
            let icon = UIImage(); //GET ICON FROM jid
            let time = Date().UTCString()
            
            tableViewArray.append(MessageView(string:body, time:time, icon:icon, outgoing:true, read:false))
            
            let mess = Message(string: "")
            mess.messageID = messageID
            mess.from = "MYSELF"
            mess.date = Date().UTCString()
            mess.body = body
            
            chatHistory.add(mess.string)
            
            buddy.saveHistory(chatHistory)

            let indexPath = IndexPath(row: chatHistory.count - 1, section: 0)
            self.tableView.insertRows(at: [indexPath] , with: UITableViewRowAnimation.right)
            self.tableView.scrollToRow(at: indexPath, at: UITableViewScrollPosition.bottom, animated:true)
            textField.text = ""
         }
         
      }
      return true
   }
   
   
   func tapRemoveKeyboard() {
      textField.resignFirstResponder()
   }


   func keyboardWillShow(_ notif : Notification) {
      let keyb_rect = (notif.userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
      if keyb_rect.origin.y < view.frame.size.height {
         animateViewMoving(keyb_rect.size.height, up:true)
         keyboard_moved = true
      }
   }
   
   func keyboardWillHide(_ notif : Notification) {
      let keyb_rect = (notif.userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
      if keyboard_moved {
         animateViewMoving(keyb_rect.size.height, up:false)
         keyboard_moved = false
      }
   } 
   
   
   func animateViewMoving (_ moveValue :CGFloat, up:Bool){
      
      let movementDuration:TimeInterval = 0.3
      UIView.beginAnimations( "animateView", context: nil)
      UIView.setAnimationBeginsFromCurrentState(false)
      UIView.setAnimationDuration(movementDuration )
      
      if up {
         tableView.frame.size.height = tableViewRect.height-moveValue
         bottomPanel.frame.origin.y = bottomPanel.frame.origin.y - moveValue
         if tableView.contentOffset.y > 0 {
            tableView.contentOffset.y = tableView.contentOffset.y + moveValue
         }
         
      } else {
         tableView.frame = tableViewRect
         bottomPanel.frame.origin.y = bottomPanel.frame.origin.y + moveValue
      }
      
      UIView.commitAnimations()
   }

}








