//
//  XMPPClient.swift
//  Jabbernaut
//
//  Created by Mikhail Baynov on 27/07/16.
//  Copyright © 2016 Mikhail Baynov. All rights reserved.
//
import Foundation


open class XMPPClient : NSObject {
   
   var jabberconnector : JabberConnector!
   
   var model : XMPPModel!
   var authmodel : XMPPAuthModel!
   var parser : XMPPParser!
   var sender : XMPPSender!

   
   
   func connectXMPP(_ server : String, tls : Bool) {
      
      jabberconnector = JabberConnector()

      model = XMPPModel()
      model.server = server
      model.login = btcmanager.getXMPPLogin(fromAddress: appdata.addressString, server: model.server)
      model.password = btcmanager.getXMPPPassword(fromSecretHEXString: appdata.secretHEXString, server: model.server)
      print("model.login = " + model.login)
      print("model.password = " + model.password)
      
      authmodel = XMPPAuthModel()
      parser = XMPPParser()
      sender = XMPPSender()
      
      if tls {
         jabberconnector.connectTLS(model.server)
      } else {
         jabberconnector.connect(model.server)
      }
   }
   
   func disconnectXMPP() {
      jabberconnector.disconnect()
      jabberconnector = nil
      model = nil
      authmodel = nil
      parser = nil
      sender = nil
   }

}
