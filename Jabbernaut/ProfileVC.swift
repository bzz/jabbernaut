import UIKit

class ProfileVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, QRCodeReaderDelegate {
    
   var exitButton = UIButton()
   var statsButton = UIButton()

   
   var controller : ProfileVC?
   
   var bedButton : UIButton!

   var userInfo : Buddy!
   
   var avatarButton : UIButton!
   var nameField : TagField!
   var positionField : TagField!
   var QRCodeButton : UIButton!
   
   var parentVC : NavigationVC!
   override func viewDidLoad() {
      parentVC = parent as! NavigationVC
      view.frame = parentVC.mainViewBed.bounds
      
      view.backgroundColor = UIColor(hexA:0xffffff25)
//      view.backgroundColor = UIColor(hex: 0xf3f3f3)

      let btnHeight :CGFloat = 49 * UIScreen.main.bounds.width / 320
      let btnWidth  :CGFloat = 64 * UIScreen.main.bounds.width / 320

      
      userInfo = Buddy(string: appdata.userInfoString)
      
      
      avatarButton = UIButton(frame: CGRect(x: 0, y: 0, width: 120, height: 120))
      avatarButton.backgroundColor = UIColor(hex: 0xffffff)
      avatarButton.layer.cornerRadius = avatarButton.frame.width/2.0
      avatarButton.layer.masksToBounds = true
      avatarButton.setImage(resizeImage(appdata.userAvatar, newWidth: avatarButton.frame.size.width), for: UIControlState())
      avatarButton.addTarget(self, action:#selector(avatarButtonPressed), for: .touchUpInside)
      view.addSubview(avatarButton)
      
      
      nameField = TagField(frame: CGRect(x: 120, y: 0, width: view.frame.width-120, height: 30))
      nameField.label.text = " NAME"
      nameField.textField.addTarget(self, action: #selector(textFieldEditingFinished(_:)), for: UIControlEvents.editingDidEndOnExit)
      nameField.placeholder = "Enter your name"
      nameField.text = userInfo.name
      view.addSubview(nameField)
      
      positionField = TagField(frame: CGRect(x: 120, y: 30, width: view.frame.width-120, height: 30))
      positionField.label.text = " TITLE"
      positionField.textField.addTarget(self, action: #selector(textFieldEditingFinished(_:)), for: UIControlEvents.editingDidEndOnExit)
      positionField.placeholder = "Describe yourself"
      positionField.text = userInfo.position
      view.addSubview(positionField)
      
      
      
      let jidLabel = UILabel(frame: CGRect(x: 0, y: 120, width: view.frame.width, height: 30))
      jidLabel.backgroundColor = UIColor(hex: 0xf7f7f7)
      jidLabel.textAlignment = .center
      jidLabel.text = xmppclient.model.jid
      jidLabel.textColor = UIColor(hex: 0x000000)
      jidLabel.font = UIFont(name: "HelveticaNeue-Thin", size: 18)
      view.addSubview(jidLabel)
      
      let addressLabel = UILabel(frame: CGRect(x: 0, y: 150, width: view.frame.width, height: 30))
      addressLabel.backgroundColor = UIColor(hex: 0xf7f7f7)
      addressLabel.textAlignment = .center
      addressLabel.text = appdata.addressString
      addressLabel.textColor = UIColor(hex: 0x000000)
      addressLabel.font = UIFont(name: "Courier", size: 12)
      view.addSubview(addressLabel)
      
      
      QRCodeButton = UIButton(frame: CGRect(x: 0, y: 200, width: view.frame.height - 200 - btnHeight,
                                            height: view.frame.height - 200 - btnHeight))
      var str = "bitcoin:" + appdata.addressString + "?jid=" + xmppclient.model.jid
      if userInfo.name != ""
         { str += "&name=" + userInfo.name }
      if appdata.APNSToken != ""
         { str += "&APNStoken=" + appdata.APNSToken }
      
      if let image = generateQRCode(from: str) {
         QRCodeButton.setImage(resizeImage(image, newWidth: QRCodeButton.frame.size.width), for: .normal)
      }
      QRCodeButton.addTarget(self, action:#selector(QRCodeButtonPressed), for: .touchUpInside)
      view.addSubview(QRCodeButton)

      
      exitButton = UIButton(type: UIButtonType.custom)
      exitButton.frame = CGRect(x: view.frame.width - btnWidth, y: view.frame.height-btnHeight, width: btnWidth, height: btnHeight)
      exitButton.backgroundColor = UIColor(hex: 0xf7f7f7)
      exitButton.setBackgroundImage(UIImage(named: "icon_exit"), for: UIControlState())
      exitButton.adjustsImageWhenHighlighted = false
      exitButton.addTarget(self, action:#selector(exitButtonPressed), for: UIControlEvents.touchUpInside)
      view.addSubview(exitButton)
      
      

      parentVC.removeAddButton()
    
      parentVC.setupTitleButton("Profile", titleColor: UIColor(hex: 0x000000), font: UIFont(name: "HelveticaNeue-Bold", size: 16)!, backgroundColor: UIColor(hex: 0xf7f7f7), target:self)
   }
   
   

   func exitButtonPressed() {
      appdata.saveAll()
      xmppclient.disconnectXMPP()
      appDelegate.window?.rootViewController = SecretKeyVC()
   }
   
   
   func avatarButtonPressed() {
      let vc = UIImagePickerController()
      vc.delegate = self
      vc.allowsEditing = true
      vc.sourceType = .photoLibrary
      self.present(vc, animated: true, completion: nil)
   }
   
   func QRCodeButtonPressed() {
      let vc = QRCodeReaderVC()
      vc.delegate = self
      self.present(vc, animated: true, completion: nil)
   }
   
   
   func imagePickerController(_ picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
      let resized_image = resizeImage(image, newWidth: 40)
      appdata.userAvatar = resized_image
      avatarButton.setImage(resizeImage(resized_image, newWidth: avatarButton.frame.size.width), for: UIControlState())
      xmppclient.sender.sendVCARD()
      picker.dismiss(animated: true, completion: nil)
   }
   
   func resizeImage(_ image: UIImage, newWidth: CGFloat) -> UIImage {
      let scale = newWidth / image.size.width
      let newHeight = image.size.height * scale
      UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
      image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
      if let newImage = UIGraphicsGetImageFromCurrentImageContext() {
         UIGraphicsEndImageContext()
         return newImage
      } else {
         return UIImage()
      }
   }
   
   
   func textFieldEditingFinished(_ field: UITextField) {
      userInfo.name = nameField.text
      userInfo.position = positionField.text
      appdata.userInfoString = userInfo.string
      xmppclient.sender.sendVCARD()
      field.resignFirstResponder()
   }
    

   func generateQRCode(from string: String) -> UIImage? {
      let data = string.data(using: String.Encoding.ascii)
      
      if let filter = CIFilter(name: "CIQRCodeGenerator") {
         filter.setValue(data, forKey: "inputMessage")
         if let output = filter.outputImage {
            return UIImage(ciImage: output)
         }
      }
      
      return nil
   }
   
   
   func gotQRCode(code:String) {
      var addr : String = ""
      var jid : String = ""
      var name : String = ""
      var APNStoken : String = ""
      if let a = URLComponents(string: code)?.path { addr = a }
      if let a = URLComponents(string: code)?.queryItems?.first(where: { $0.name == "jid" })?.value { jid = a }
      if let a = URLComponents(string: code)?.queryItems?.first(where: { $0.name == "name" })?.value { name = a }
      if let a = URLComponents(string: code)?.queryItems?.first(where: { $0.name == "APNStoken" })?.value { APNStoken = a }
      let buddyString = addr + SEPARATOR + jid + SEPARATOR  + name + SEPARATOR + SEPARATOR + APNStoken
      appdata.buddies[addr] = buddyString
      appdata.saveAll()

      
      xmppclient.sender.subscribePresence(jid)
      xmppclient.sender.requestSubscription(jid)

   }
}
