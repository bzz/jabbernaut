import UIKit


class ConversationCell: UITableViewCell {
   var view : ConversationView!
   
   override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
      super.init(style: style, reuseIdentifier: reuseIdentifier)
      self.backgroundColor = UIColor.clear
      self.selectionStyle = .none
   }
   
   required init?(coder aDecoder: NSCoder) {
      super.init(coder: aDecoder)
   }
}


class ConversationView: UIView {
   var onlineIndicator : UIView!
   var iconView : UIImageView!
   var titleLabel : UILabel!
   var secondLabel : UILabel!
   var stringLabel : UILabel!
   var dateLabel : UILabel!
   
   var jid : String!   
   
   init(key : String) {
      super.init(frame: CGRect(x:0, y:0, width:UIScreen.main.bounds.width, height:80))

      let buddy = Buddy(key: key)
      self.jid = buddy.last_jid
      
      self.backgroundColor = UIColor(hex: 0xffffff)

      
      onlineIndicator = UIView(frame:CGRect(x:13, y:13, width:45, height:45))
      onlineIndicator.layer.cornerRadius = onlineIndicator.frame.size.width/2
      if xmppclient.model.isOnline(buddy.last_jid) {
         onlineIndicator.backgroundColor = UIColor(hex: 0x7ce055)
      } else {
         onlineIndicator.backgroundColor = UIColor(hex: 0xa9a9a9)
      }
      self.addSubview(onlineIndicator)
      
      iconView = UIImageView(frame:CGRect(x:16, y:16, width:39, height:39))
      iconView.backgroundColor = UIColor(hex: 0xf3f3f3)
      iconView.layer.cornerRadius = iconView.frame.width/2.0
      iconView.layer.masksToBounds = true
      if let avatar_string = appdata.avatars[appdata.buddiesKeyForJID(jid)] as? String {
         let img_data = Data.init(base64Encoded: avatar_string, options: .ignoreUnknownCharacters)
         iconView.image = UIImage(data: img_data!)
      }
      self.addSubview(iconView)

      
      titleLabel = UILabel(frame:CGRect(x:65, y:7, width:UIScreen.main.bounds.width - 85, height:24))
      titleLabel.textColor = UIColor(hex: 0x000000)
      titleLabel.font = UIFont(name: "HelveticaNeue-Bold", size: 13)
      titleLabel.text = buddy.screen_name
      self.addSubview(titleLabel)
      
      secondLabel = UILabel(frame:CGRect(x:65, y:30, width:UIScreen.main.bounds.width - 85, height:24))
      secondLabel.textColor = UIColor(hex: 0xa9a9a9)
      secondLabel.font = UIFont(name: "HelveticaNeue", size: 11)
      if buddy.address == "" {
         secondLabel.text = buddy.last_jid
      } else {
         secondLabel.text = buddy.address
      }
      self.addSubview(secondLabel)
     
      
      if let lastMessage = buddy.history().lastObject as? String {
         let mess = Message(string: lastMessage)
         
         let paragraphStyle = NSMutableParagraphStyle()
         paragraphStyle.lineSpacing = 4;
         let attributes = [NSParagraphStyleAttributeName: paragraphStyle]
         stringLabel = UILabel(frame:CGRect(x:65, y:61, width:UIScreen.main.bounds.width - 85, height:24))
         stringLabel.numberOfLines = 0
         stringLabel.attributedText = NSAttributedString(string:mess.body, attributes: attributes)
         stringLabel.textColor = UIColor(hex: 0xa9a9a9)
         stringLabel.font = UIFont(name: "HelveticaNeue", size: 13)
         stringLabel.sizeToFit()
         stringLabel.numberOfLines = stringLabel.frame.height > 60 ? 2 : stringLabel.numberOfLines
         stringLabel.sizeToFit()
         self.addSubview(stringLabel)
         
         
         let df = DateFormatter()
         df.timeZone = TimeZone(abbreviation: "UTC")
         df.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
         var dstr = ""
         if let d = df.date(from: mess.date) {
            dstr = d.dateStringWithFormat("dd.MM.yyyy")
         }
         
         dateLabel = UILabel(frame:CGRect(x:254, y:8, width:UIScreen.main.bounds.width - 85, height:14))
         dateLabel.numberOfLines = 0
         dateLabel.text = dstr
         dateLabel.textColor = UIColor(hex: 0x5e99d9)
         dateLabel.font = UIFont(name: "HelveticaNeue", size: 10)
         self.addSubview(dateLabel)
         
         
         self.frame = CGRect(x:0, y:0, width:UIScreen.main.bounds.width, height:stringLabel.frame.height + 80)
         
         
         let separator = UIView(frame: CGRect(x:0, y:self.frame.height-1, width:UIScreen.main.bounds.width, height:1))
         separator.backgroundColor = UIColor(hex: 0xd1d0d4)
         self.addSubview(separator)
         
         
      }
      
   }
   
   required init?(coder aDecoder: NSCoder) {
      super.init(coder: aDecoder)
   }
}
