import UIKit
import Security


let hexButtonSide = screenWidth / 8
let keyWidth : CGFloat = screenWidth / 5
let keyHeight : CGFloat = screenWidth / 5 / 3 * 2

let digitHeight : CGFloat = screenWidth / 16


class SecretKeyVC : UIViewController, HexInputDelegate {
   
   var bedButton : UIButton!
   
   var addressLabel = UILabel()
   var hexScreen : HexInput!
   
   
   override func viewDidLoad() {
      super.viewDidLoad()
      
      
      //578 vs 480 height
      //bed
      bedButton = UIButton(frame: CGRect(x: 0, y: 70, width: screenWidth, height: 104))
      bedButton.backgroundColor = UIColor(hex: 0xf7f7f7)
      bedButton.addTarget(self, action:#selector(WelcomeVC.bedButtonPressed), for: UIControlEvents.touchDown)
      bedButton.layer.borderColor = UIColor(hex: 0xdcdcdc).cgColor
      bedButton.layer.borderWidth = 1
      view.addSubview(bedButton)
      
      if screenHeight > 480 {
         let welcomeLabel = UILabel(frame: CGRect(x: 88, y: 70, width: screenWidth-176, height: 90))
         welcomeLabel.text = "Welcome to \nJABBERNAUT"
         welcomeLabel.numberOfLines = 2
         welcomeLabel.textAlignment = .center
         welcomeLabel.textColor = UIColor(hex: 0x000000)
         welcomeLabel.font = UIFont(name: "HelveticaNeue-Thin", size: 24)
         view.addSubview(welcomeLabel)
         
         bedButton.frame.origin.y = 170
      }
      
      
      let textLabel = UILabel(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 30))
      textLabel.backgroundColor = UIColor(hex: 0xf7f7f7)
      textLabel.textAlignment = .center
      textLabel.text = "Your ID:"
      textLabel.textColor = UIColor(hex: 0x000000)
      textLabel.font = UIFont(name: "HelveticaNeue-Thin", size: 21)
      bedButton.addSubview(textLabel)
      
      addressLabel = UILabel(frame: CGRect(x: 0, y: 30, width: screenWidth, height: 30))
      addressLabel.backgroundColor = UIColor(hex: 0xf7f7f7)
      addressLabel.textAlignment = .center
      addressLabel.text = appdata.addressString
      addressLabel.textColor = UIColor(hex: 0x000000)
      addressLabel.font = UIFont(name: "Courier", size: 12)
      bedButton.addSubview(addressLabel)
      
      let proceedButton = UIButton(frame: CGRect(x: 0, y: 60, width: screenWidth, height: 44))
      proceedButton.setTitle("PROCEED", for: UIControlState())
      proceedButton.setTitleColor(UIColor.black, for: UIControlState())
      proceedButton.titleLabel?.font = UIFont(name: "HelveticaNeue-Regular", size: 18)
      proceedButton.backgroundColor = UIColor(hex: 0xf9f9f9)
      proceedButton.addTarget(self, action:#selector(proceedButtonPressed), for: UIControlEvents.touchUpInside)
      bedButton.addSubview(proceedButton)
      
      hexScreen = HexInput()
      hexScreen.delegate = self
      view.addSubview(hexScreen)
      
   }
   
   
   func proceedButtonPressed() {
      if appdata.addressString != "" {
         appDelegate.proceedToNavigation()
      }
   }
   
   func bedButtonPressed() {
      appdata.randomizeSecretHEXString()
      appdata.clearCache()
      addressLabel.text = appdata.addressString
      hexScreen.reloadHexScreen()
   }
   
   
   func hexStringChanged(_ string:String) {
      addressLabel.text = appdata.addressString
   }
}





protocol HexInputDelegate {
   func hexStringChanged(_ string:String)
}
class HexInput : UIView {
   
   var hexOffsetOrigin = CGPoint(x: 0, y: 0) {
      didSet {
         hexSelectionView.frame.origin = hexOffsetOrigin
         hexScreen.addSubview(hexSelectionView)
      }
   }
   
   
   
   
   var hexOffset : Int = 0 {
      didSet {
         let y = hexOffset / 16
         let x = hexOffset % 16 / 2 * 2
         hexOffsetOrigin = CGPoint(x: CGFloat(x) * digitHeight, y: CGFloat(y) * digitHeight)
      }
   }
   var hexSelectionView : UIView!
   
   
   let hexScreenHeight : CGFloat = 5 * digitHeight
   var hexScreen : UIView!
   
   var delegate : HexInputDelegate!
   
   init() {
      super.init(frame: CGRect(x: 0, y: screenHeight - hexScreenHeight - 4 * keyHeight, width: screenWidth, height: hexScreenHeight + 4 * keyHeight))
      
      let keybbg = UIImageView(frame: CGRect(x: 0, y: hexScreenHeight, width: screenWidth, height: 4 * keyHeight))
      keybbg.image = UIImage(named:"hex_keyboard")
      self.addSubview(keybbg)
      self.backgroundColor = UIColor.white
      hexScreen = UIView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: hexScreenHeight))
      self.addSubview(hexScreen)
      hexSelectionView = UIView(frame: CGRect(x: CGFloat(hexOffsetOrigin.x), y: CGFloat(hexOffsetOrigin.y), width: digitHeight * 2, height: digitHeight))
      hexSelectionView.backgroundColor = UIColor(hexA: 0x00ffee90)
      hexScreen.addSubview(hexSelectionView)
      
      
      
      reloadHexScreen()
   }
   required init?(coder aDecoder: NSCoder) {
      super.init(coder: aDecoder)
   }
   
   
   func reloadHexScreen() {
      for v in hexScreen.subviews {
         v.removeFromSuperview()
      }
      
      for y in (0..<4) {
         for i in (0..<8) {
            var name1 = "letter"
            name1.append(appdata.secretHEXString[appdata.secretHEXString.characters.index(appdata.secretHEXString.startIndex, offsetBy: y*16 + i*2)])
            let im1 = UIImageView(image:UIImage(named: name1))
            
            im1.frame = CGRect(x: CGFloat(i) * 2 * digitHeight + digitHeight/5,
                               y: CGFloat(y) * digitHeight + digitHeight/10,
                               width: digitHeight * 4/5,
                               height: digitHeight * 4/5)
            self.addSubview(im1)
            var name2 = "letter"
            name2.append(appdata.secretHEXString[appdata.secretHEXString.characters.index(appdata.secretHEXString.startIndex, offsetBy: y*16 + 1 + i*2)])
            let im2 = UIImageView(image:UIImage(named: name2))
            im2.frame = CGRect(x: CGFloat(i) * 2 * digitHeight + digitHeight,
                               y: CGFloat(y) * digitHeight + digitHeight/10,
                               width: digitHeight * 4/5,
                               height: digitHeight * 4/5)
            self.addSubview(im2)
            
            im1.backgroundColor = UIColor(hex: 0xff3b30)
            im2.backgroundColor = UIColor(hex: 0xff3b30)
         }
      }
   }
   
   
   override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
      super.touchesBegan(touches , with:event)
      for touch in touches {
         let t = touch.location(in: self)
         var out = ""
         
         if t.y/digitHeight < 4 {
            let x = Int(t.x/digitHeight/2)
            let y = Int(t.y/digitHeight)
            hexOffset = y*16 + x*2
         }
         
         if t.x <= keyWidth {
            switch t.y {
            case hexScreenHeight..<hexScreenHeight + keyHeight:
               out = "1"
            case hexScreenHeight + keyHeight..<hexScreenHeight + keyHeight * 2:
               out = "6"
            case hexScreenHeight + keyHeight * 2..<hexScreenHeight + keyHeight * 4:
               if hexOffset > 1 {
                  hexOffset = hexOffset / 2 * 2 - 2
               }
            default:
               break
            }
         }
         if t.x >= keyWidth && t.x < keyWidth * 2 {
            switch t.y {
            case hexScreenHeight..<hexScreenHeight + keyHeight:
               out = "2"
            case hexScreenHeight + keyHeight..<hexScreenHeight + keyHeight * 2:
               out = "7"
            case hexScreenHeight + keyHeight * 2..<hexScreenHeight + keyHeight * 3:
               out = "A"
            case hexScreenHeight + keyHeight * 3..<hexScreenHeight + keyHeight * 4:
               out = "D"
            default:
               break
            }
         }
         if t.x >= keyWidth * 2 && t.x < keyWidth * 3 {
            switch t.y {
            case hexScreenHeight..<hexScreenHeight + keyHeight:
               out = "3"
            case hexScreenHeight + keyHeight..<hexScreenHeight + keyHeight * 2:
               out = "8"
            case hexScreenHeight + keyHeight * 2..<hexScreenHeight + keyHeight * 3:
               out = "B"
            case hexScreenHeight + keyHeight * 3..<hexScreenHeight + keyHeight * 4:
               out = "E"
            default:
               break
            }
         }
         if t.x >= keyWidth * 3 && t.x < keyWidth * 4 {
            switch t.y {
            case hexScreenHeight..<hexScreenHeight + keyHeight:
               out = "4"
            case hexScreenHeight + keyHeight..<hexScreenHeight + keyHeight * 2:
               out = "9"
            case hexScreenHeight + keyHeight * 2..<hexScreenHeight + keyHeight * 3:
               out = "C"
            case hexScreenHeight + keyHeight * 3..<hexScreenHeight + keyHeight * 4:
               out = "F"
            default:
               break
            }
         }
         if t.x >= keyWidth * 4 {
            switch t.y {
            case hexScreenHeight..<hexScreenHeight + keyHeight:
               out = "5"
            case hexScreenHeight + keyHeight..<hexScreenHeight + keyHeight * 2:
               out = "0"
            case hexScreenHeight + keyHeight * 2..<hexScreenHeight + keyHeight * 4:
               if hexOffset < 62 {
                  hexOffset = hexOffset / 2 * 2 + 2
               }
            default:
               break
            }
         }
         
         if out != "" {
            
            
            let range :  Range<String.Index> = appdata.secretHEXString.characters.index(appdata.secretHEXString.startIndex, offsetBy: hexOffset)..<appdata.secretHEXString.characters.index(appdata.secretHEXString.startIndex, offsetBy: hexOffset + 1)
            appdata.secretHEXString.replaceSubrange(range, with: out)
            if hexOffset < 63 {
               hexOffset += 1
            }
            
            reloadHexScreen()
            hexOffsetOrigin = CGPoint(x: hexOffsetOrigin.x, y: hexOffsetOrigin.y) //to invoke setter
            
            //   //tmp - del
            //            hexString = "16260783e40b16731673622ac8a5b045fc3ea4af70f727f3f9e92bdd3a1ddc42"
            
            
            self.delegate.hexStringChanged(appdata.secretHEXString)
         }
      }
   }
   
}






