//
//  BTCMan.m
//  Jabbernaut
//
//  Created by Mikhail Baynov on 16/06/16.
//  Copyright © 2016 Mikhail Baynov. All rights reserved.
//

#import "BTCManager.h"

#import "src/ecc.h"
#import "src/btc_hash.h"
#import "src/libbase58.h"


@implementation BTCManager


- (NSString *)addressFromSecretKey:(NSData *)secretKey {

   uint8_t pub_key65[65];
   uECC_get_public_key65((uint8_t*) secretKey.bytes, pub_key65);
   
   uint8_t digest20[20];
   hash160(pub_key65, 65, digest20);
   

   uint8_t in[25];
   uint8_t hash[21];
   uint8_t *p = in;
#if BITCOIN_TESTNET
   p[0] = 111;
#else
   p[0] = 0;
#endif
   memcpy(p+1, digest20, 20);
   NSMutableData *result = [NSMutableData dataWithBytes:in length:25];
   sha256(in, 21, hash);
   sha256(hash, 32, hash);
   memcpy((uint8_t *)result.bytes+21, hash, 4);
   
   
   
   NSLog(@"\n key %@ \n result %@\n\n", secretKey, result);

   
   
//   uint8_t *bin; //= hex from binary
//   size_t bin_len;
//   
//   NSData *resultData = [NSData dataWithBytes:result.bytes length:result.length];

   
   char *out = calloc(100, 1);
   base58_encode(result.bytes, result.length, out);
   
   NSLog(@"btcmanager: %@", [NSString stringWithUTF8String:out]);
   return [NSString stringWithUTF8String:out];
}

/*
- (NSData *)sha256_2plus:(NSData *)digest20 {
   uint8_t in[25];
   uint8_t hash[21];
   uint8_t *p = in;
#if BITCOIN_TESTNET
   p[0] = 111;
#else
   p[0] = 0;
#endif
   memcpy(p+1, (uint8_t *)digest20.bytes, 20);
   NSMutableData *data = [NSMutableData dataWithBytes:in length:25];
   memcpy(in, data.bytes, 21);
   sha256(in, 21, hash);
   sha256(hash, 32, hash);
   memcpy((uint8_t *)data.bytes+21, hash, 4);
   return data;
}
*/




- (NSString *)getXMPPLoginFromAddress:(NSString *)address server:(NSString *)server {
   NSData *base = [[NSString stringWithFormat:@"%@%@", address, server] dataUsingEncoding:NSUTF8StringEncoding];
   uint8_t *digest = calloc(20, 1);
   hash160(base.bytes, base.length, digest);
   
   char *out = calloc(1000, 1);
   base58_encode(digest, 20, out);

   out[8] = 0;
   
   for (size_t i = 0; i < strlen(out); ++i) {
      uint8_t p = out[i];
      switch (p) {
         case '0':
         case '1':
            out[i] = 'a';
            break;
         case '2':
         case '3':
            out[i] = 'e';
            break;
         case '4':
         case '5':
            out[i] = 'i';
            break;
         case '6':
         case '7':
            out[i] = 'o';
            break;
         case '8':
         case '9':
            out[i] = 'u';
            break;
            
         default:
            break;
      }
   }
   return [[NSString stringWithUTF8String:out] lowercaseString];
}


- (NSString *)getXMPPPasswordFromSecretHEXString:(NSString *)hexString server:(NSString *)server {
   NSData *base = [[NSString stringWithFormat:@"%@%@", hexString, server] dataUsingEncoding:NSUTF8StringEncoding];
   uint8_t *digest = calloc(20, 1);
   hash160(base.bytes, base.length, digest);
   
   char *out = calloc(100, 1);
   base58_encode(digest, 20, out);
   out[10] = 0;
   return [NSString stringWithUTF8String:out];
}


- (BOOL)isValidAddress:(NSString *)string {
   
   
   const char* text = string.UTF8String;
   size_t text_len = string.length;// [string lengthOfBytesUsingEncoding:NSUTF8StringEncoding];
   uint8_t *bin = calloc(1000, 1);
   size_t binsz = 25;
   b58tobin(bin, &binsz, text, text_len);
   
//   NSData *data = [NSData dataWithBytes:bin length:binsz];
//   NSLog(@"%@", data);
   uint8_t *hash = calloc(21, 1);
   sha256(bin, 21, hash);
   sha256(hash, 32, hash);

   if (memcmp(hash, &bin[21], 4) == 0) {
      return YES;
   }
   return NO;
}


@end
