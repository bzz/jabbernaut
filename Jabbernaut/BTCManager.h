//
//  BTCMan.h
//  Jabbernaut
//
//  Created by Mikhail Baynov on 16/06/16.
//  Copyright © 2016 Mikhail Baynov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BTCManager : NSObject {}


@property (nonatomic) dispatch_queue_t Q;


- (NSString *)addressFromSecretKey:(NSData *)secretKey;
- (BOOL)isValidAddress:(NSString *)string;

- (NSString *)getXMPPLoginFromAddress:(NSString *)address server:(NSString *)server;
- (NSString *)getXMPPPasswordFromSecretHEXString:(NSString *)hexString server:(NSString *)server;


@end
