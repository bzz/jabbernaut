import UIKit

class FriendsVC: UIViewController {
    
   var controller : FriendsVC?
   var parentVC : NavigationVC!
   
   var tableView = UITableView()
   var tableViewArray: [FriendView] = []

   var searchView = UIView()
   var searchField = UITextField()
   
   
   required init() {
      super.init(nibName: nil, bundle: nil)
   }
   required init(coder aDecoder: NSCoder) {
      super.init(coder: aDecoder)!
   }
   
   
   override func viewDidAppear(_ animated: Bool) {
      super.viewDidAppear(animated)
      xmppclient.sender.getRoster()
   }
   
   override func viewDidLoad() {
      super.viewDidLoad()
      parentVC = parent as! NavigationVC
      view.frame = parentVC.mainViewBed.bounds

      
      
      searchView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 44))
      searchView.backgroundColor = UIColor(hex:0xc2c2c5)
      
      UITextField.appearance().tintColor = UIColor(hex:0xc2c2c5)
      searchField = UITextField()
      searchField.placeholder = "Add friend: name@server.com"
      searchField.frame = CGRect(x: 7, y: 7, width: view.frame.width-14, height: 30)
      searchField.layer.cornerRadius = 5
      searchField.backgroundColor = UIColor.white
      searchField.textAlignment = .center
      searchField.autocapitalizationType = .none
      searchField.autocorrectionType = .no
      searchField.keyboardType = .emailAddress
      searchField.delegate = self
      
      searchView.addSubview(searchField)
      view.addSubview(searchView)
      
      
      
      tableView = UITableView(frame: CGRect(x: 0, y: 44, width: view.frame.width, height: view.frame.height - 44))
      tableView.delegate = self
      tableView.dataSource = self
      tableView.register(FriendCell.self, forCellReuseIdentifier: String(describing: FriendCell.self))
      tableView.backgroundColor = UIColor.clear
      tableView.showsVerticalScrollIndicator = false
      tableView.separatorStyle = .none
      tableView.rowHeight = UITableViewAutomaticDimension;
      
      view.addSubview(tableView)
      
      
      parentVC.removeAddButton()
      parentVC.setupTitleButton("Friends", titleColor: UIColor(hex: 0x000000), font: UIFont(name: "HelveticaNeue-Bold", size: 16)!, backgroundColor: UIColor(hex: 0xf7f7f7), target:self)

      
      
      
      reloadTableView()
      initNotifications()
      
      print("appdata.buddies")
      print(appdata.buddies)
   }
   
   
   
   func initNotifications() {
      Notifier.addObserver(self, event: .newContactAdded)
      
      Notifier.addObserver(self, event: .friendOnline)
      Notifier.addObserver(self, event: .friendOffline)
      Notifier.addObserver(self, event: .friendAway)
      Notifier.addObserver(self, event: .gotVCARD)
   }
   
   deinit {
      Notifier.removeObserver(self, event: .newContactAdded)
      
      Notifier.removeObserver(self, event: .friendOnline)
      Notifier.removeObserver(self, event: .friendOffline)
      Notifier.removeObserver(self, event: .friendAway)
      Notifier.removeObserver(self, event: .gotVCARD)
   }
   
   
   
   func newContactAdded(_ notif : Notification) {
      reloadTableView()
   }
   
   
   func friendOnline(_ notif : Notification) {
      reloadTableView()
   }
   func friendAway(_ notif : Notification) {
      reloadTableView()
   }
   func friendOffline(_ notif : Notification) {
      reloadTableView()
   }
   func gotVCARD(_ notif : Notification) {
      reloadTableView()
   }
   
   
   
   
   func reloadTableView() {
      tableViewArray.removeAll()
      for key in appdata.buddies.allKeys {
         tableViewArray.append(FriendView(key:key as! String))
      }
      tableView.reloadData()
   }
   
   
   
}


extension FriendsVC: UITableViewDelegate, UITableViewDataSource {
   
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return tableViewArray.count
   }
   
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: FriendCell.self)) as! FriendCell
      cell.view = tableViewArray[indexPath.row]
      cell.addSubview(cell.view)
      return cell
   }
   
   func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
      let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: FriendCell.self)) as! FriendCell
      cell.view.removeFromSuperview()
   }
   
   
   func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
      return tableViewArray[indexPath.row].frame.size.height
   }
   
   
   func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      let jid = tableViewArray[indexPath.row].jid
      
      appDelegate.window?.rootViewController = ChatVC(jid:jid!)
   }
   
   
   func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
      if (editingStyle == UITableViewCellEditingStyle.delete) {
         tableView.beginUpdates()
         xmppclient.sender.unsubscribePresence(tableViewArray[indexPath.row].jid)
         appdata.history.removeObject(forKey: tableViewArray[indexPath.row].jid)
         appdata.buddies.removeObject(forKey: tableViewArray[indexPath.row].jid)
         tableViewArray.remove(at: indexPath.row)
         tableView.deleteRows(at: [indexPath], with: .fade)
         tableView.endUpdates()
         
      }
   }
   
}


extension FriendsVC: UITextFieldDelegate {
   func textFieldShouldReturn(_ textField: UITextField) -> Bool {
      textField.resignFirstResponder()
      if (textField.text == "") {
         return false
      }
      
      var text = textField.text!
      if text.range(of: "@") == nil {
         text = text + "@" + xmppclient.model.server
      }      
      xmppclient.sender.requestSubscription(text)
      
      
      textField.text = ""
      
      return true
   }
   
}
