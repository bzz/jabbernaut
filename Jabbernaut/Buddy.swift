import Foundation


open class Buddy {
   var address = ""
   var last_jid = ""
   var name = ""
   var position = ""
   var APNSToken = ""
   
   var screen_name : String {
      var out = name
      if name != "" && position != "" {
         out += ", "
      }
      if position != "" {
         out += position
      }
      return out
   }
   
   init(string : String) {
      let arr = NSMutableArray(array: string.components(separatedBy: SEPARATOR))
      if arr.count > 4 {
         self.address = arr[0] as! String
         self.last_jid = arr[1] as! String
         self.name = arr[2] as! String
         self.position = arr[3] as! String
         self.APNSToken = arr[4] as! String
      }
   }
   
   
   convenience init() {
      self.init(string: SEPARATOR + SEPARATOR + SEPARATOR + SEPARATOR + SEPARATOR + SEPARATOR + SEPARATOR + SEPARATOR + SEPARATOR + SEPARATOR + SEPARATOR)
   }
   

   convenience init(key : String) {
      if let str = appdata.buddies[key] as? String {
         self.init(string : str)
         return
      }
      
      let out = appdata.buddiesKeyForJID(key)
      
      
      if let o = appdata.buddies[out] {
         self.init(string : o as! String)
      } else {
         self.init()
      }
      
   }
   

   var string : String {
      get {
         return address + SEPARATOR + last_jid + SEPARATOR + name + SEPARATOR + position + SEPARATOR + APNSToken
      }
   }
   
   
   var vCard : String {
      get {
         //TODO: MUST NOT RETURN appdata.APNSToken, LOCAL TOKEN INSTEAD
         return "<iq id='v2' type='set'><vCard xmlns='vcard-temp'><FN>\(name)</FN><N><FAMILY/><GIVEN/><MIDDLE></MIDDLE></N><NICKNAME>\(name)</NICKNAME><BDAY/><TEL><WORK/><VOICE/><NUMBER></NUMBER></TEL><ADR><WORK>\(appdata.addressString)</WORK><LOCALITY/><PCODE>\(appdata.APNSToken)</PCODE></ADR><EMAIL><INTERNET/><PREF/><USERID></USERID></EMAIL><JABBERID>\(last_jid)</JABBERID><DESC>\(position)</DESC><PHOTO><BINVAL>\(appdata.userAvatarString)</BINVAL></PHOTO></vCard></iq>"
      }
   }
   
   
   open func save() {
      //TODO:  make sure is a VALID adressString
      if self.address != "" {
         appdata.buddies[self.address] = self.string
         appdata.buddies[self.last_jid] = nil
         return
      }
      if self.last_jid != "" {
         appdata.buddies[self.last_jid] = self.string
      }
   }
   
   open func remove() {
      appdata.buddies[self.address] = nil
      appdata.buddies[self.last_jid] = nil
   }
   
   
   open func history() -> NSMutableArray {
      if let chatHistory = appdata.history[self.address] as? NSMutableArray {
         
         for key in appdata.history.allKeys {
            if self.address == appdata.buddiesKeyForJID(key as! String) {
               
               appdata.history.setValue(chatHistory, forKey: self.address)
               appdata.history.setValue(nil, forKey: key as! String)
               return chatHistory
            }
         }
         return chatHistory
      }
      if let arr = appdata.history[self.last_jid] as? NSMutableArray {
         return arr
      }
      return NSMutableArray()
   }

   open func saveHistory(_ history : NSArray) {
      if self.address != "" {
         appdata.history[self.address] = history
      } else {
         appdata.history[self.last_jid] = history
      }
   }

}
