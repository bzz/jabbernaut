import Foundation


class Message { 
   var date = ""
   var from = ""   //jid for them, address for yourself
   var body = ""
   var messageID = ""
   
   
   init(string : String) {
      let arr = NSMutableArray(array: string.components(separatedBy: SEPARATOR))
      if arr.count > 3 {
         self.date = arr[0] as! String
         self.from = arr[1] as! String
         self.body = arr[2] as! String
         self.messageID = arr[3] as! String
      }
   }
   

   var string : String {
      get {
         return date + SEPARATOR + from + SEPARATOR + body + SEPARATOR + messageID
      }
   }
   
      
}
