//
//  XMLReader.m
//  XMLReader
//
//  Created by Mikhail Baynov on 30/05/16.
//  Copyright © 2016 Mikhail Baynov. All rights reserved.
//

#import "XMLReader.h"



static int string_contains_char(const char* str, const char ch) {
   for(size_t x = 0; x < strlen(str); x++) {
      if(str[x] == ch) {
         return 1;
      }
   }
   return 0;
}





@interface XMLReader() {
   
   
}

@property (nonatomic, strong) NSMutableArray <NSString *> *stringsList;


@property (nonatomic, strong) NSMutableArray <NSMutableDictionary *> *dictionaryStack;






@end

@implementation XMLReader

+ (instancetype)shared {
   static XMLReader *sharedInstance = nil;
   static dispatch_once_t onceToken;
   
   dispatch_once(&onceToken, ^{
      sharedInstance = [[XMLReader alloc] init];
      sharedInstance.text = @"";
      sharedInstance.TEXTNODE_KEY = @"TEXTNODE_KEY";
      sharedInstance.MAX_BUFFER_SIZE = 400000;
   });
   return sharedInstance;
}




- (NSDictionary *)dictionaryForXMLString:(NSString *)string {
   NSDictionary *out = [NSDictionary new];
   NSString *backupText = self.text;
   self.text = @"";
   [self addText:string];
   out = self.dictionaryFromText;
   self.text = backupText;
   return out;
}


- (NSDictionary *)dictionaryFromText {
   if (self.dictionaryStack.count == 1) {
      NSDictionary *out = self.dictionaryStack[0];
      self.text = @"";
      return out;
   } else {
      return nil;
   }
}


- (NSMutableArray *)addText:(NSString *)string {
   if (self.text == nil) {
      self.text = @"";
   }
   self.text = [self.text stringByAppendingString:string];
   
   self.stringsList = [NSMutableArray new];
   
   const char* text = self.text.UTF8String;
   size_t text_len = [self.text lengthOfBytesUsingEncoding:NSUTF8StringEncoding];
   
   uint8_t* element = calloc(1000, 1);

//   NSMutableString *element_text_wrapper;
   uint8_t* element_text = calloc(_MAX_BUFFER_SIZE, 1);
   
   //   NSLog(@"IN: %@\n", string);
   
   boolean_t element_began = false;
   boolean_t header_began = false;
   size_t element_index = 0;
   size_t element_text_index = 0;
   
   
   
   uint8_t curr_char;
   uint8_t prev_char;
   uint8_t next_char;
   
   
   for (size_t offset = 0; offset < strlen(text); ++offset) {
      
      curr_char = text[offset];
      if (offset <= 0) {
         prev_char = '\0';
      } else prev_char = text[offset-1];
      if (offset >= text_len) {
         next_char = '\0';
      } else next_char = text[offset+1];
      
      
      if (curr_char == '<') {
         if (next_char == '/') {
            element_began = false;
            element_text[element_text_index] = '\0';
            element_text_index = 0;
            if (element_text[0] != '\0') {
               [self.stringsList removeLastObject];
               [self.stringsList addObject:[NSString stringWithFormat:@"OPEN %s %@=%@", element, self.TEXTNODE_KEY, [NSString stringWithUTF8String: element_text]]];
            }
         }
         element_began = true;
         header_began = true;
         continue;
      }
      
      if (curr_char == '>') {
         header_began = false;
         element[element_index] = '\0';
         
         if (element[element_index-1] == '/') {
            element[element_index-1] = '\0';
            [self.stringsList addObject:[NSString stringWithFormat:@"EMPTY %s", element]];
            element_index = 0;
            continue;
         }
         
         if (element[0] == '/') {
            element += 1;
            [self.stringsList addObject:[NSString stringWithFormat:@"CLOSE %s", element]];
            element_index = 0;
            continue;
         }
         
         if (element[0] != '\0') {
            [self.stringsList addObject:[NSString stringWithFormat:@"OPEN %s", element]];
            element_index = 0;
            continue;
         }
         
         
         if (prev_char == '/') {
            element_began = false;
            element_text[element_text_index] = '\0';
            element_text_index = 0;
            if (element_text[0] != '\0') {
               [self.stringsList removeLastObject];
               [self.stringsList addObject:[NSString stringWithFormat:@"OPEN %s %@=%@", element, self.TEXTNODE_KEY, [NSString stringWithUTF8String: element_text]]];
            }
         }
         continue;
      }
      
      if (element_began) {
         if (header_began) {
            element[element_index] = curr_char;
            ++element_index;
         } else {
            element_text[element_text_index] = curr_char;
            ++element_text_index;
         }
      }
      
   }
   
   
   
   [self processDicStringList];
   
   return nil;
}




- (NSArray <NSString *>*)splitString:(NSString *)string {
   //   NSLog(@"SPLIT STRING: %@", string);
   
   NSMutableArray *out = [NSMutableArray new];
   
   const char* text = string.UTF8String;
   size_t text_len = [string lengthOfBytesUsingEncoding:NSUTF8StringEncoding];
   
   char* str = calloc(_MAX_BUFFER_SIZE, 1);
   size_t str_offset = 0;
   
   
   bool processing_textnode_key = false;
   
   NSString *textnode_key_plus = [self.TEXTNODE_KEY stringByAppendingString:@"="];
   const char *textnode_key_plus_str = textnode_key_plus.UTF8String;
   for (size_t i = 0; i < text_len; ++i) {
      str[str_offset] = text[i];
      ++str_offset;
      if (text[i] == ' ' && processing_textnode_key == false) {
         str[str_offset-1] = '\0';
         [out addObject:[NSString stringWithUTF8String:str]];
         memset(str, 0, _MAX_BUFFER_SIZE);
//         str = calloc(_MAX_BUFFER_SIZE, 1);
         str_offset = 0;
      }
      
      if (strlen(textnode_key_plus_str) == strlen(str))
         if (strcmp(str, textnode_key_plus_str) == 0) {
            processing_textnode_key = true;
         }
      
   }
   str[str_offset] = '\0';
   [out addObject:[NSString stringWithUTF8String:str]];
   
   //   NSLog(@"array contents: [");
   //   for (NSString *s in out) {
   //      NSLog(@"%@", s);
   //   }
   //   NSLog(@"]");
   return out;
}








- (void)processDicStringList {
   //   NSLog(@"dicStringList::::: \n%@", self.stringsList);
   
   
   self.dictionaryStack = [NSMutableArray new];
   [self.dictionaryStack addObject:[NSMutableDictionary new]];
   
   
   for (size_t i = 0; i < self.stringsList.count; ++i) {
      NSArray *parts = [self splitString:self.stringsList[i]];
      NSMutableDictionary *parentDic = self.dictionaryStack.lastObject;
      NSMutableDictionary *childDic = [NSMutableDictionary new];
      if (parts.count < 2) continue;
      NSString *element = parts[1];
      if ([parts[0] isEqualToString:@"OPEN"]) {
         
//ignore opening stream tags
         if ([parts[1] isEqualToString:@"?xml"]) continue;
         if ([parts[1] isEqualToString:@"stream:stream"]) continue;
         
         
         
         for (size_t i = 2; i < parts.count; ++i) {
            NSArray *keyValue = [self getKeyValuePairFromString:parts[i]];
            if (keyValue.count < 2) continue;
            [childDic setValue:keyValue[1] forKey:keyValue[0]];
         }
         
         NSObject *existingValue = parentDic[element];
         if (existingValue) {
            NSMutableArray *arr = [NSMutableArray new];
            if ([existingValue isKindOfClass:[NSMutableArray class]]) {
               arr = (NSMutableArray *)existingValue;
            } else {
               arr = [NSMutableArray new];
               [arr addObject:existingValue];
               [parentDic setObject:arr forKey:element];
            }
            [arr addObject:childDic];
         } else {
            [parentDic setObject:childDic forKey:element];
         }
         
         [self.dictionaryStack addObject:childDic];
         
         
         
      } else if ([parts[0] isEqualToString:@"CLOSE"]) {
         if (self.dictionaryStack.count > 1) {
            [self.dictionaryStack removeLastObject];
         } else {
            NSLog(@"XMLReader ERROR:  Extra closing tag");
         }
         
         
      } else if ([parts[0] isEqualToString:@"EMPTY"]) {
         for (size_t i = 2; i < parts.count; ++i) {
            NSArray *keyValue = [self getKeyValuePairFromString:parts[i]];
            if (keyValue.count < 2) continue;
            [childDic setValue:keyValue[1] forKey:keyValue[0]];
         }
         [parentDic setObject:childDic forKey:element];
         
      }
      
      
      //      NSLog(@"self.dictionaryStack    %@", self.dictionaryStack);
   }
   //   NSLog(@"self.dictionaryStack [0]   %@", [self.dictionaryStack objectAtIndex:0]);
   
}



- (NSArray *)getKeyValuePairFromString:(NSString *)string {
   NSMutableArray *out = [NSMutableArray new];
   for (size_t i = 0; i < string.length; ++i) {
      if ([string characterAtIndex:i] == '=') {
         out[0] = [string substringToIndex:i];
         out[1] = [string substringFromIndex:i+1];
         break;
      }
   }
   if (out.count < 2) return nil;
   for (size_t i = 0; i < 2; ++i) {
      out[i] = [out[i] stringByReplacingOccurrencesOfString:@"\"" withString:@""];
      out[i] = [out[i] stringByReplacingOccurrencesOfString:@"'" withString:@""];
   }
   return out;
   
}




@end