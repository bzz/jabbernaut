import UIKit



open class XMPPAuthModel: NSObject {

   
   var jabber_authorised = false
   var jabber_bound_resource = false
//   var jabber_got_session = false
   var jabber_ready : Bool {
      get {
         if jabber_authorised && jabber_bound_resource {
            return true
         } else {
            return false
         }
      }
      set {
         jabber_authorised = false
         jabber_bound_resource = false
//         jabber_got_session = false
      }
   }
 
   var DIGEST_MD5_AUTH_AVAILABLE = false
   var PLAIN_AUTH_AVAILABLE = false
   var SCRAM_SHA1_AUTH_AVAILABLE = false
   

   
   
   override init() {
      super.init()
      
      Notifier.addObserver(self, event: .registrationSucceed)
      Notifier.addObserver(self, event: .registrationFailedTooFast)
      Notifier.addObserver(self, event: .registrationFailedTaken)
      Notifier.addObserver(self, event: .registrationFailedNotAllowed)
      
      Notifier.addObserver(self, event: .authMechanismsFound)
      Notifier.addObserver(self, event: .authSucceed)
      Notifier.addObserver(self, event: .authFailed)
      
      Notifier.addObserver(self, event: .bindFeatureFound)
      Notifier.addObserver(self, event: .boundResource)
      Notifier.addObserver(self, event: .gotSession)
      Notifier.addObserver(self, event: .proceedWithTLS)
      
      Notifier.addObserver(self, event: .tlsFeatureFound)
      Notifier.addObserver(self, event: .tlsRequired)
   }
   
   
   
   deinit {

      Notifier.removeObserver(self, event: .registrationSucceed)
      Notifier.removeObserver(self, event: .registrationFailedTooFast)
      Notifier.removeObserver(self, event: .registrationFailedTaken)
      Notifier.removeObserver(self, event: .registrationFailedNotAllowed)
      
      Notifier.removeObserver(self, event: .authMechanismsFound)
      Notifier.removeObserver(self, event: .authSucceed)
      Notifier.removeObserver(self, event: .authFailed)
      
      Notifier.removeObserver(self, event: .bindFeatureFound)
      Notifier.removeObserver(self, event: .boundResource)
      Notifier.removeObserver(self, event: .gotSession)
      Notifier.removeObserver(self, event: .proceedWithTLS)

      Notifier.removeObserver(self, event: .tlsFeatureFound)
      Notifier.removeObserver(self, event: .tlsRequired)
   }
   
   
   
   func registrationSucceed(_ notif : Notification) {
      print("registrationSucceed for server:")
      if let server = notif.userInfo?["server"] as? String {
         xmppclient.sender.authenticate()
         print(server)
      }
   }
   
   
   func registrationFailedTooFast(_ notif : Notification) {
      print("registrationFailedTooFast for server:")
      if let server = notif.userInfo?["server"] as? String {
         let status = "Wait then register"
         appdata.servers[server] = status
         Notifier.post(.serverStatusStringChanged, userInfo: ["status" : status])
      }
   }
   
   
   func registrationFailedTaken(_ notif:Notification) {
      print("registrationFailedTaken for server:")
      if let server = notif.userInfo?["server"] as? String {
         xmppclient.sender.authenticate()
         print(server)
      }
   }
   
   
   func registrationFailedNotAllowed(_ notif : Notification) {
      print("registrationFailedNotAllowed for server:")
      if let server = notif.userInfo?["server"] as? String {
         //         xmppclient.sender.authenticate()
         print(server)
      }
   }
   
   
   func authMechanismsFound(_ notif : Notification) {
      if let arr = notif.userInfo?["mechanisms"] as? NSArray {
         for m in arr {
            if m as! String == "DIGEST-MD5" {
               xmppclient.authmodel.DIGEST_MD5_AUTH_AVAILABLE = true
            }
            if m as! String == "PLAIN" {
               xmppclient.authmodel.PLAIN_AUTH_AVAILABLE = true
            }
            if m as! String == "SCRAM-SHA-1" {
               xmppclient.authmodel.SCRAM_SHA1_AUTH_AVAILABLE = true
            }
         }
      }
      xmppclient.sender.send_registration_info()
   }
   

   func authSucceed(_ notif:Notification) {
      print("authSucceed for server:")
      if let server = notif.userInfo?["server"] as? String {
         print(server)
      }
      xmppclient.authmodel.jabber_authorised = true
      xmppclient.sender.getSession()
   }
   
   func authFailed(_ notif : Notification) {
      print("authFailed for server:")
      if let server = notif.userInfo?["server"] as? String {
         print(server)
      }
   }
   
   
   func bindFeatureFound(_ notif : Notification) {
      xmppclient.sender.bindResource("\(xmppclient.model.resource)")
   }
   
   func boundResource(_ notif : Notification) {
      xmppclient.authmodel.jabber_bound_resource = true
      if jabber_authorised {
         Notifier.post(.jabberReady)
      }
      xmppclient.sender.startSession()
      xmppclient.sender.sendVCARD()
      xmppclient.sender.showPresence()
   }

   
   func gotSession(_ notif : Notification) {
//      xmppclient.authmodel.jabber_got_session = true
//      xmppclient.sender.sendVCARD()
//      xmppclient.sender.showPresence()
   }
   
   
   
   func tlsFeatureFound(_ notif : Notification) {
      print("TLS FEATURE FOUND")
   }
   
   func tlsRequired(_ notif : Notification) {
      print ("TLS REQUIRED")
      xmppclient.sender.enqueue("<starttls xmlns='urn:ietf:params:xml:ns:xmpp-tls'/>")
   }
   
   func proceedWithTLS(_ notif : Notification) {
      print("NOW PROCEED WITH TLS")
      
      xmppclient.jabberconnector.use_tls = true
      xmppclient.jabberconnector.disconnect()
//      xmppclient.connectXMPP(xmppclient.model.server, tls: true)
//      xmppclient.jabberconnector.disconnect()

//      xmppclient.jabberconnector.connectTLS(xmppclient.model.server)
//      xmppclient.jabberconnector.startTLS()
      
//xmppclient.sender.enqueue("<?xml version='1.0'?><stream:stream xmlns:stream='http://etherx.jabber.org/streams' xmlns='jabber:client' to='\(xmppclient.model.server)' version='1.0'>")
//      xmppclient.jabberconnector.current_socket.readDataWithTimeout(10, tag: 0)

      ///      //send over ssl/tls connection
      ///      xmppclient.sender.enqueue("<stream:stream xmlns='jabber:client' xmlns:stream='http://etherx.jabber.org/streams' to='example.com' version='1.0'>")
   }
   

   
}




